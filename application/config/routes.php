<?php

// auth
$route['^(en|tr|pr|fr)/register'] = 'auth/register_page';
$route['^(en|tr|pr|fr)/logout'] = 'auth/logout';
$route['^(en|tr|pr|fr)/verify/(:any)'] = 'auth/verify/$1/$2';
$route['^(en|tr|pr|fr)/recovery'] = 'auth/recovery_page';
$route['^(en|tr|pr|fr)/change-password/(:any)'] = 'auth/change_password/$1/$2';
$route['^(en|tr|pr|fr)/login'] = 'auth/login_page';
$route['^(en|tr|pr|fr)/change-email/(:any)'] = 'auth/change_email/$1/$2';

// accounts
$route['^(en|tr|pr|fr)/generator'] = 'accounts/index';
$route['^(en|tr|pr|fr)/generate/(:any)'] = 'accounts/generate/$1/$2';

// pages
$route['^(en|tr|pr|fr)/404'] = 'pages/not_found_page';

// admin
$route['^(en|tr|pr|fr)/admin/settings'] = 'admin/settings_page';
$route['^(en|tr|pr|fr)/admin'] = 'admin/index';
$route['^(en|tr|pr|fr)/admin/categories'] = 'admin/categories_page';
$route['^(en|tr|pr|fr)/admin/platforms'] = 'admin/platforms_page';
$route['^(en|tr|pr|fr)/admin/accounts'] = 'admin/accounts_page';
$route['^(en|tr|pr|fr)/admin/pages'] = 'admin/pages_page';
$route['^(en|tr|pr|fr)/mail'] = 'mail/mail_send';

// user
$route['^(en|tr|pr|fr)/user/settings'] = 'user/index';

// categories
$route['^(en|tr|pr|fr)/category/(:any)'] = 'categories/index/$1/$2';

// platforms
$route['^(en|tr|pr|fr)/platform/(:any)'] = 'platforms/index/$1/$2';

// misc
$route['^(en|tr|pr|fr)/sitemap\.xml'] = 'misc/sitemap';

$route['^en/(.+)$'] = "$1";
$route['^tr/(.+)$'] = "$1";
$route['^pr/(.+)$'] = "$1";
$route['^fr/(.+)$'] = "$1";
$route['^(en|tr|pr|fr)$'] = $route['default_controller'];

$route['default_controller'] = 'misc/coming_soon_page';
$route['404_override'] = 'pages/not_found_page';
$route['translate_uri_dashes'] = FALSE;

// misc
$route['^(en|tr|pr|fr)/sitemap\.xml'] = 'misc/sitemap';
