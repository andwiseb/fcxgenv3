<?php

function ip() {
    $ip = '';

    if (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
    } else if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
    } else if (getenv('HTTP_X_FORWARDED')) {
        $ip = getenv('HTTP_X_FORWARDED');
    } else if (getenv('HTTP_FORWARDED_FOR')) {
        $ip = getenv('HTTP_FORWARDED_FOR');
    } else if (getenv('HTTP_FORWARDED')) {
       $ip = getenv('HTTP_FORWARDED');
    } else if (getenv('REMOTE_ADDR')) {
        $ip = getenv('REMOTE_ADDR');
    } else {
        $ip = 'UNKNOWN';
    }

    return $ip;
}

function email_control($email) {
    $t = get_instance();
    $t->load->model('auth_model');

    return $t->auth_model->email_control($email);
}

function email_user_info($email) {
    $t = get_instance();
    $t->load->model('auth_model');

    return $t->auth_model->email_user_info($email);
}

function id_user_info($id) {
    $t = get_instance();
    $t->load->model('auth_model');

    return $t->auth_model->id_user_info($id);
}

function random_str($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';

    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}

function get_settings() {
    $t = get_instance();
    $t->load->model('admin_model');

    return $t->admin_model->get_settings();
}

function send_mail($who, $message, $subject) {   
    $t = get_instance();
    $t->load->library('email');

    $config['protocol']  = 'smtp';
    $config['smtp_host'] = "mail.cosvex.com";
    $config['smtp_user'] = "noreply@cosvex.com";
    $config['smtp_pass'] = "aslaolmaz1234";
    $config['smtp_port'] = "587";
    $config['mailtype']  = 'html';

    $t->email->initialize($config);  
    $t->email->from('noreply@cosvex.com', 'Fcxgen');
    $t->email->to($who);
    $t->email->subject($subject);
    $t->email->message($message);

    return $t->email->send();
}

function send_mail_html($who, $view, $subject, $data) {   
    $t = get_instance();
    $t->load->library('email');

    $config['protocol']  = 'smtp';
    $config['smtp_host'] = "mail.cosvex.com";
    $config['smtp_user'] = "noreply@cosvex.com";
    $config['smtp_pass'] = "aslaolmaz1234";
    $config['smtp_port'] = "587";
    $config['mailtype']  = 'html';

    $message = $t->load->view($view, $data, TRUE);

    $t->email->initialize($config);  
    $t->email->from('noreply@cosvex.com', 'Fcxgen');
    $t->email->to($who);
    $t->email->subject($subject);
    $t->email->message($message);

    return $t->email->send();
}

function get_platforms_with_category($category_id) {   
    $t = get_instance();
    $t->load->model('accounts_model');

    return $t->accounts_model->get_platforms_with_category($category_id);
}

function get_meta_tag($url) {   
    $t = get_instance();
    $t->load->model('misc_model');

    return $t->misc_model->get_meta_tag($url);
}