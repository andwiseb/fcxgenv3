<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends MY_Controller {
	public function __construct() {
		parent::__construct();
		
		
		$this->load->model('auth_model');
    }

	public function register_page() {
		$data['title'] = $this->lang->line('kayit_ol');

		$this->load->view('auth/register', $data);
	}

	public function register() {
		// POST VAR MI?
		if (isset($_POST['submit'])) {
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$confirm_password = $this->input->post('confirm-password');

			// ŞİFRELER EŞLEŞİYOR MU?
			if ($password == $confirm_password) {
				if (strlen($password) < 8) {
					$this->session->set_flashdata('register_message', $this->lang->line('en_az_sekiz_karakter'));
	
					redirect(site_url('register'));
				} else {
					// E-POSTA KAYITLI MI?
					if (email_control($email) == true) {
						$data = array(
							'email' => $email,
							'password' => password_hash($password, PASSWORD_DEFAULT),
							'ip' => ip(),
							'code' => random_str(30)
						);

						// REGİSTER İŞLEMİ TAMAMSA
						if ($this->auth_model->register($data)) {
							$verify_url = site_url('verify/') . $data['code'];

							send_mail($data['email'], $verify_url, $this->lang->line('üye_aktivasyonu'));

							$this->session->set_flashdata('login_message', $this->lang->line('kayit_tamam'));
	
							redirect(site_url('login'));
						} else {
							$this->session->set_flashdata('register_message', $this->lang->line('kayit_hatasi'));
	
							redirect(site_url('register'));
						}
					} else {
						$this->session->set_flashdata('register_message', $this->lang->line('eposta_var'));
	
						redirect(site_url('register'));
					}
				}
			} else {
				$this->session->set_flashdata('register_message', $this->lang->line('sifreler_uyusmuyor'));

				redirect(site_url('register'));
			}
		} else {
			show_404();
		}
	}

	public function login_page() {
		$data['title'] = $this->lang->line('giris_yap');

		$this->load->view('auth/login', $data);
	}

	public function login() {
		// POST VAR MI?
		if (isset($_POST['submit'])) {
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$data = array(
				'email' => $email,
				'password' => $password
			);

			// LOGİN İŞLEMİ OKEYSE
			if ($this->auth_model->login($data)) {
				// SESSION EKLEME
				$this->session->set_userdata('id', email_user_info($email)->id);

				redirect(site_url());
			} else {
				redirect(site_url('login'));
			}
		} else {
			show_404();
		}
	}

	public function logout() {
		if ($this->session->userdata('id') == '') {
			redirect(site_url('404'));
		} else {
			$this->session->unset_userdata('id');
			$this->session->set_flashdata('login_message', $this->lang->line('hoscakal'));

			redirect(site_url('login'));
		}
	}

	public function verify($language, $code) {
		if ($code != '') {
			if ($this->auth_model->verify($code)) {
				$this->session->set_flashdata('login_message', $this->lang->line('aktivasyon_tamam'));

				redirect(site_url('login'));			
			} else {
				$this->session->set_flashdata('login_message', $this->lang->line('aktivasyon_hatasi'));

				redirect(site_url('login'));
			}
		} else {
			show_404();
		}
	}

	public function recovery_page() {
		$data['title'] = $this->lang->line('sifre_sifirla');

		$this->load->view('auth/recovery', $data);
	}

	public function recovery() {
		// POST VAR MI?
		if (isset($_POST['submit'])) {
			$email = $this->input->post('email');

			$data = array(
				'email' => $email,
				'code' => random_str(30)
			);
			
			if (email_control($email) != true) {
				if ($this->auth_model->recovery($data)) {
					$recovery_mail = site_url('change-password/') . $data['code'];

					send_mail($email, $recovery_mail, $this->lang->line('sifre_sifirla'));

					$this->session->set_flashdata('recovery_message', $this->lang->line('sifre_sifirlama_istegi'));

					redirect(site_url('recovery'));
				} else {
					$this->session->set_flashdata('recovery_message', $this->lang->line('sifre_sifirlama_hatasi'));

					redirect(site_url('recovery'));
				}
			} else {
				$this->session->set_flashdata('recovery_message', $this->lang->line('eposta_yok'));

				redirect(site_url('recovery'));
			}
		} else {
			show_404();
		}
	}

	public function change_password($language, $code = '') {
		if (isset($_POST['submit'])) {
			$password = $this->input->post('password');
			$confirm_password = $this->input->post('confirm-password');
			$code = $this->input->post('code');

			if ($password == $confirm_password) {
				if (strlen($password) < 8) {
					$this->session->set_flashdata('change_password_message', $this->lang->line('en_az_sekiz_karakter'));
	
					redirect(site_url('change-password/') . $code);
				} else {
					$data = array(
						'password' => $password,
						'confirm_password' => $confirm_password,
						'code' => $code
					);

					$this->session->set_flashdata('change_password', 1);

					if ($this->auth_model->change_password($data)) {
						$this->session->set_flashdata('login_message', $this->lang->line('sifre_degistirme_tamam'));

						redirect(site_url('login'));			
					} else {
						$this->session->set_flashdata('change_password_message', $this->lang->line('sifre_degistirme_hatasi'));

						redirect(site_url('change-password/') . $code);
					}
				}
			} else {
				$this->session->set_flashdata('change_password_message', $this->lang->line('sifreler_uyusmuyor'));

				redirect(site_url('change-password/') . $code);
			}
		} else {
			if ($code != '') {
				$data['code'] = $code;
				
				if ($this->auth_model->change_password($data)) {
					$data = array(
						'title' => $this->lang->line('sifre_degistir'),
						'code' => $code
					);

					$this->load->view('auth/change_password', $data);			
				} else {
					$this->session->set_flashdata('login_message', $this->lang->line('gecersiz_istek'));

					redirect(site_url('login'));
				}
			} else {
				show_404();
			}
		}
	}

	public function change_email($language, $code) {
		if ($code != '') {
			if ($this->auth_model->change_email($code)) {
				$this->session->set_flashdata('settings_message', $this->lang->line('eposta_degistirme_tamam'));

				redirect(site_url('user/settings'));			
			} else {
				$this->session->set_flashdata('settings_message', $this->lang->line('eposta_degistirme_hatasi'));

				redirect(site_url('user/settings'));
			}
		} else {
			show_404();
		}
	}
}
