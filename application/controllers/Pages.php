<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends MY_Controller {
	public function __construct() {
		parent::__construct();
    }

	public function not_found_page() {
		$data['title'] = $this->lang->line('sayfa_bulunamadi');

		$this->load->view('404', $data);
	}
}
