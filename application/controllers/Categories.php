<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Categories extends MY_Controller {
	public function __construct() {
		parent::__construct();

		if ($this->session->userdata('id') == '') {
			redirect(site_url('404'));
		}

		$this->load->model('categories_model');
	}

	public function index($language, $url) {
		$data = array(
			'title' => $this->categories_model->get_title($url),
			'categories' => $this->categories_model->get_categories($url)
		);

		if ($data['categories']->num_rows() > 0) {
			$this->load->view('categories/index', $data);
		} else {
			redirect(site_url('404'));
		}
	}
}
