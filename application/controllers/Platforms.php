<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Platforms extends MY_Controller {
	public function __construct() {
		parent::__construct();

		if ($this->session->userdata('id') == '') {
			redirect(site_url('404'));
		}

		$this->load->model('platforms_model');
	}

	public function index($language, $url) {
		$data = array(
			'title' => $this->platforms_model->get_title($url),
			'platform' => $this->platforms_model->get_platforms($url)
		);

		if ($data['platform'] != '') {
			$this->load->view('platforms/index', $data);
		} else {
			redirect(site_url('404'));
		}
	}
}
