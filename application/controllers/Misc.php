<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require_once './vendor/autoload.php';
require './vendor/instagram/vendor/autoload.php';

use Phpfastcache\Helper\Psr16Adapter;

class Misc extends MY_Controller {
	public function __construct() {
		parent::__construct();
		
		
		$this->load->model('misc_model');
		$this->load->helper('email');
	}

	public function coming_soon_page() {
		$data['title'] = $this->lang->line('cok_yakinda');

		$this->load->view('misc/coming_soon', $data);
	}

	public function subscribe() {
		if (isset($_POST['email'])) {
			$data = $this->input->post('email');

			if (!valid_email($data)) {
				$this->session->set_flashdata('subscribe', $this->lang->line('eposta_gecersiz'));

				redirect(site_url());
			} else {
				if ($this->misc_model->subscribe($data)) {
					$this->session->set_flashdata('subscribe', $this->lang->line('abone_olma_tamam'));

					redirect(site_url());
				} else {

					$this->session->set_flashdata('subscribe', $this->lang->line('abone_olma_hatasi'));
					redirect(site_url());
				}
			}
		} else {
			redirect(site_url('404'));
		}
	}

	public function reset_share() {
		$this->misc_model->reset_share();
	}

	// public function mail() {
	// 	$data = array(
	// 		'link' => '',
	// 		'title' => 'allahu ekber',
	// 		'description' => ''
	// 	);

	// 	send_mail_html('ulasckan@gmail.com', 'admin/mail', $this->lang->line('sifre_sifirla'), $data);
	// }

	public function sitemap() {
		$data = array(
			'pages' => $this->misc_model->sitemap(),
			'languages' => array('en', 'tr', 'pr', 'fr')
		);

		header("Content-Type: text/xml;charset=iso-8859-1");

		$this->load->view('misc/sitemap', $data);
	}

	public function ig_control() {
		$instagram = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), 'sellthingco', 'Aslaolmaz1234.', new Psr16Adapter('Files'));
		$instagram->login();

		sleep(2);

		$username = 'sellthingco';
		$list = [];
		$account = $instagram->getAccount($username);

		sleep(1);

		$list = $instagram->getFollowing($account->getId(), 1000, 100, true);

		if ($this->misc_model->ig_control($list)) {
			redirect(site_url('404'));
		}
	}
}