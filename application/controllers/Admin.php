<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MY_Controller {
	public function __construct() {
		parent::__construct();
		
		if ($this->session->userdata('id') == '') {
			redirect(site_url('404'));
		} else {
			

			if (id_user_info($this->session->userdata('id'))->auth != 1) {
				redirect(site_url('404'));
			} else {
				$this->load->model('admin_model');
				$this->load->helper('form');
			}
		}
    }

	public function settings_page() {
		$data = array(
			'title' => $this->lang->line('ayarlar'),
			'settings' => $this->admin_model->get_settings()
		);
		
		$this->load->view('admin/settings', $data);
	}

	public function update_settings() {
		if (isset($_POST['submit'])) {	
			$data = array (
				'title' => $this->input->post('site-title'),
				'description' => $this->input->post('site-description')
			);

			if (!empty($_FILES['site-logo']['name'])) {
				$config = array(
					'upload_path' => './resources/assets/img/',
					'allowed_types' => 'jpg|jpeg|png|gif',
					'file_name' => random_str()
				);
				
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('site-logo')) {
					$this->session->set_flashdata('settings_message', $this->lang->line('logo_yuklenme_hatasi'));

					redirect(site_url('admin/settings'));
				} else {
					$data['logo'] = $this->upload->data('file_name');
				}
			}

			if (!empty($_FILES['site-favicon']['name'])) {
				$config = array(
					'upload_path' => './resources/assets/img/',
					'allowed_types' => 'jpg|jpeg|png|gif',
					'file_name' => random_str()
				);
				
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('site-favicon')) {
					$this->session->set_flashdata('settings_message', $this->lang->line('favicon_yuklenme_hatasi'));

					redirect(site_url('admin/settings'));
				} else {
					$data['favicon'] = $this->upload->data('file_name');
				}
			}

			if (!empty($_FILES['meta-image']['name'])) {
				$config = array(
					'upload_path' => './resources/assets/img/',
					'allowed_types' => 'jpg|jpeg|png|gif',
					'file_name' => random_str()
				);
				
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('meta-image')) {
					$this->session->set_flashdata('settings_message', $this->lang->line('meta_resim_yuklenme_hatasi'));

					redirect(site_url('admin/settings'));
				} else {
					$data['meta_image'] = $this->upload->data('file_name');
				}
			}

			if ($this->admin_model->update_settings($data)) {
				$this->session->set_flashdata('settings_message', $this->lang->line('ayarlar_guncellendi'));

				redirect(site_url('admin/settings'));
			} else {
				$this->session->set_flashdata('settings_message', $this->lang->line('ayar_guncelleme_hatasi'));

				redirect(site_url('admin/settings'));
			}
		}
	}

	public function index() {
		$data['title'] = $this->lang->line('yonetici_paneli');
		
		$this->load->view('admin/index', $data);
	}

	public function categories_page() {
		$data = array(
			'title' => $this->lang->line('kategoriler'),
			'categories' => $this->admin_model->get_categories()
		);
		
		$this->load->view('admin/categories', $data);
	}

	public function add_category() {
		if (isset($_POST['submit'])) {
			$data = array (
				'title' => $this->input->post('category-name'),
				'url' => $this->input->post('category-url'),
				'meta_title' => $this->input->post('meta-title'),
				'meta_description' => $this->input->post('meta-description'),
				'meta_keywords' => $this->input->post('meta-keywords')
			);

			if ($this->admin_model->add_category($data)) {
				$this->session->set_flashdata('categories_message', $this->lang->line('kategori_eklendi'));

				redirect(site_url('admin/categories'));
			} else {
				redirect(site_url('admin/categories'));
			}
		} else {
			redirect(site_url('404'));
		}
	}

	public function delete_category() {
		if (isset($_POST['id'])) {
			$data = $this->admin_model->delete_category($_POST['id']);

        	echo json_encode($data);
		} else {
			redirect(site_url('404'));
		}
	}

	public function edit_category() {
		if (isset($_POST['id'])) {
			$result = $this->admin_model->edit_category($_POST['id']);
			$data = array(
				'id' => $result->c_id,
				'title' => $result->c_title,
				'url' => $result->url,
				'meta_title' => $result->m_title,
				'meta_description' => $result->description,
				'meta_keywords' => $result->keywords
			);

			echo json_encode($data);
		} else {
			redirect(site_url('404'));
		}
	}

	public function update_category() {
		if (isset($_POST['submit'])) {
			$data = array (
				'id' => $this->input->post('category-id'),
				'title' => $this->input->post('category-name'),
				'url' => $this->input->post('category-url'),
				'meta_title' => $this->input->post('meta-title'),
				'meta_description' => $this->input->post('meta-description'),
				'meta_keywords' => $this->input->post('meta-keywords')
			);

			if ($this->admin_model->update_category($data)) {
				$this->session->set_flashdata('categories_message', $this->lang->line('kategori_guncellendi'));

				redirect(site_url('admin/categories'));
			} else {
				$this->session->set_flashdata('categories_message', $this->lang->line('kategori_guncelleme_hatasi'));

				redirect(site_url('admin/categories'));
			}
		} else {
			redirect(site_url('404'));
		}
	}

	public function platforms_page() {
		$data = array(
			'title' => $this->lang->line('platformlar'),
			'platforms' => $this->admin_model->get_platforms(),
			'categories' => $this->admin_model->get_categories()
		);
		
		$this->load->view('admin/platforms', $data);
	}

	public function add_platform() {
		if (isset($_POST['submit'])) {
			if ($this->input->post('platform-category') == '0') {
				$this->session->set_flashdata('platforms_message', $this->lang->line('kategori_seciniz'));

				redirect(site_url('admin/platforms'));
			} else {
				$data = array (
					'title' => $this->input->post('platform-name'),
					'url' => $this->input->post('platform-url'),
					'category_id' => $this->input->post('platform-category'),
					'meta_title' => $this->input->post('meta-title'),
					'meta_description' => $this->input->post('meta-description'),
					'meta_keywords' => $this->input->post('meta-keywords')
				);

				$config = array(
					'upload_path' => './resources/assets/img/',
					'allowed_types' => 'jpg|jpeg|png|gif',
					'file_name' => random_str()
				);
				
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('platform-image')) {
					$this->session->set_flashdata('platforms_message', $this->lang->line('resim_yukleme_hatasi'));

					redirect(site_url('admin/platforms'));
				} else {
					$data['image'] = $this->upload->data('file_name');
				}

				if ($this->admin_model->add_platform($data)) {
					$this->session->set_flashdata('platforms_message', $this->lang->line('platform_eklendi'));

					redirect(site_url('admin/platforms'));
				} else {
					redirect(site_url('admin/platforms'));
				}
			}
		} else {
			redirect(site_url('404'));
		}
	}

	public function delete_platform() {
		if (isset($_POST['id'])) {
			$data = $this->admin_model->delete_platform($_POST['id']);

        	echo json_encode($data);
		} else {
			redirect(site_url('404'));
		}
	}

	public function edit_platform() {
		if (isset($_POST['id'])) {
			$result = $this->admin_model->edit_platform($_POST['id']);
			$data = array(
				'id' => $result->p_id,
				'title' => $result->p_title,
				'url' => $result->url,
				'category_id' => $result->category_id,
				'meta_title' => $result->m_title,
				'meta_description' => $result->description,
				'meta_keywords' => $result->keywords,
				'image' => $result->image
			);

			echo json_encode($data);
		} else {
			redirect(site_url('404'));
		}
	}

	public function update_platform() {
		if (isset($_POST['submit'])) {
			$data = array (
				'id' => $this->input->post('platform-id'),
				'title' => $this->input->post('platform-name'),
				'url' => $this->input->post('platform-url'),
				'category_id' => $this->input->post('platform-category'),
				'meta_title' => $this->input->post('meta-title'),
				'meta_description' => $this->input->post('meta-description'),
				'meta_keywords' => $this->input->post('meta-keywords')
			);

			if (!empty($_FILES['platform-image']['name'])) {
				$config = array(
					'upload_path' => './resources/assets/img/',
					'allowed_types' => 'jpg|jpeg|png|gif',
					'file_name' => random_str()
				);
				
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('platform-image')) {
					$this->session->set_flashdata('platforms_message', $this->lang->line('resim_yukleme_hatasi'));

					redirect(site_url('admin/platforms'));
				} else {
					$data['image'] = $this->upload->data('file_name');
				}
			}

			if ($this->admin_model->update_platform($data)) {
				$this->session->set_flashdata('platforms_message', $this->lang->line('platform_guncellendi'));

				redirect(site_url('admin/platforms'));
			} else {
				$this->session->set_flashdata('platforms_message', $this->lang->line('platform_guncelleme_hatasi'));

				redirect(site_url('admin/platforms'));
			}
		} else {
			redirect(site_url('404'));
		}
	}

	public function accounts_page() {
		$data = array(
			'title' => $this->lang->line('hesaplar'),
			'platforms' => $this->admin_model->get_platforms(),
			'accounts' => $this->admin_model->get_accounts()
		);
		
		$this->load->view('admin/accounts', $data);
	}

	public function single_add_account() {
		if (isset($_POST['submit'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('mail-pass', 'Mail:pass', 'required');
			$this->form_validation->set_rules('caption', 'Caption', 'required');
			$this->form_validation->set_rules('account-platform', 'Account platform', 'required');
			$this->form_validation->set_rules('account-type', 'Account type', 'required');

			if ($this->form_validation->run() == false) {
				$this->session->set_flashdata('accounts_message', validation_errors());
			} else {
				$data = array (
					'mail_pass' => $this->input->post('mail-pass'),
					'caption' => $this->input->post('caption'),
					'platform_id' => $this->input->post('account-platform'),
					'type_id' => $this->input->post('account-type'),
					'member_id' => $this->session->userdata('id'),
					'date' => date('Y-m-d H:i:s'),
					'code' => random_str()
				);

				if (!strpos($data['mail_pass'], ':')) {
					$this->session->set_flashdata('accounts_message', $this->lang->line('iki_nokta_ust_uste_hatasi'));
		
					redirect(site_url('accounts'));
				} else {
					if ($data['platform_id'] == 0) {
						$this->session->set_flashdata('accounts_message', $this->lang->line('platform_seciniz'));
		
						redirect(site_url('accounts'));
					} else {
						if ($this->admin_model->single_add_account($data)) {
							$this->session->set_flashdata('accounts_message', $this->lang->line('hesap_eklendi'));
			
							redirect(site_url('accounts'));
						} else {
							$this->session->set_flashdata('accounts_message', $this->lang->line('hesap_ekleme_hatasi'));
			
							redirect(site_url('accounts'));
						}
					}
				}
			}
		} else {
			redirect(site_url('404'));
		}
	}

	public function bulk_add_account() {
		if (isset($_POST['submit'])) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('b-account-platform', 'Account platform', 'required');
			$this->form_validation->set_rules('b-account-type', 'Account type', 'required');

			if ($this->form_validation->run() == false) {
				$this->session->set_flashdata('accounts_message', validation_errors());
			} else {
				$data = array (
					'platform_id' => $this->input->post('b-account-platform'),
					'type_id' => $this->input->post('b-account-type'),
					'member_id' => $this->session->userdata('id'),
					'code' => random_str(15),
					'date' => date('Y-m-d H:i:s')
				);

				if ($data['platform_id'] == 0) {
					$this->session->set_flashdata('accounts_message', $this->lang->line('paylasim_kilidi'));
	
					redirect(site_url('accounts'));
				} else {
					$config['upload_path']          = './uploads/bulk';
					$config['allowed_types']        = 'pdf|doc|docx|rtf|text|txt';
					$config['file_name']             = random_str();

                	$this->load->library('upload', $config);

					if (!$this->upload->do_upload('b-accounts-file')) {
						$this->session->set_flashdata('accounts_message',  $this->upload->display_errors());
		
						redirect(site_url('accounts'));
					} else {
						$this->load->helper('file');

						$file = fopen('./uploads/bulk/' . $this->upload->data('file_name'), 'r');
						$count = 0;
						$array = array();
						
						while(!feof($file)) {
							$line = fgets($file);
							$count++;

							if (strstr($line,  '***')) {
								$data['email'] = $array[1];
								$data['password'] = $array[2];
								$data['caption'] = $array[3];

								$this->admin_model->bulk_add_account($data);

								$array[3] = ' ';
								$count = 0;
							} else {
								if ($count == 1) {
									$str_array = explode(':', $line);
									$array[$count] = $str_array[0];
									$array[$count + 1] = $str_array[1];
									$count = 2;
								} else {
									$array[$count] = $array[$count].' '.$line;
									$count = 2;
								}
							}
						}
					}

					$this->session->set_flashdata('accounts_message', $this->lang->line('hesaplar_eklendi'));
			
					redirect(site_url('accounts'));
				}
			}
		} else {
			redirect(site_url('404'));
		}
	}

	public function pages_page() {
		$data = array(
			'title' => $this->lang->line('sayfalar'),
			'pages' => $this->admin_model->get_pages()
		);
		
		$this->load->view('admin/pages', $data);
	}

	public function delete_page() {
		if (isset($_POST['id'])) {
			$data = $this->admin_model->delete_page($_POST['id']);

        	echo json_encode($data);
		} else {
			redirect(site_url('404'));
		}
	}

	public function add_page() {
		if (isset($_POST['submit'])) {
			$data = array (
				'url' => $this->input->post('page-url'),
				'meta_title' => $this->input->post('meta-title'),
				'meta_description' => $this->input->post('meta-description'),
				'meta_keywords' => $this->input->post('meta-keywords')
			);

			if (!empty($_FILES['meta-image']['name'])) {
				$config = array(
					'upload_path' => './resources/assets/img/',
					'allowed_types' => 'jpg|jpeg|png|gif',
					'file_name' => random_str()
				);
				
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('meta-image')) {
					$this->session->set_flashdata('pages_message', $this->lang->line('resim_yukleme_hatasi'));

					redirect(site_url('admin/pages'));
				} else {
					$data['meta_image'] = $this->upload->data('file_name');
				}
			}

			if ($this->admin_model->add_page($data)) {
				$this->session->set_flashdata('pages_message', $this->lang->line('sayfa_eklendi'));

				redirect(site_url('admin/pages'));
			} else {
				$this->session->set_flashdata('pages_message', $this->lang->line('sayfa_ekleme_hatasi'));

				redirect(site_url('admin/pages'));
			}
		} else {
			redirect(site_url('404'));
		}
	}

	public function edit_page() {
		if (isset($_POST['id'])) {
			$result = $this->admin_model->edit_page($_POST['id']);
			$data = array(
				'id' => $result->p_id,
				'url' => $result->url,
				'meta_title' => $result->title,
				'meta_description' => $result->description,
				'meta_keywords' => $result->keywords
			);

			echo json_encode($data);
		} else {
			redirect(site_url('404'));
		}
	}

	public function update_page() {
		if (isset($_POST['submit'])) {
			$data = array (
				'id' => $this->input->post('page-id'),
				'url' => $this->input->post('page-url'),
				'meta_title' => $this->input->post('meta-title'),
				'meta_description' => $this->input->post('meta-description'),
				'meta_keywords' => $this->input->post('meta-keywords')
			);

			if (!empty($_FILES['meta-image']['name'])) {
				$config = array(
					'upload_path' => './resources/assets/img/',
					'allowed_types' => 'jpg|jpeg|png|gif',
					'file_name' => random_str()
				);
				
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('meta-image')) {
					$this->session->set_flashdata('pages_message', $this->lang->line('resim_yukleme_hatasi'));

					redirect(site_url('admin/pages'));
				} else {
					$data['meta_image'] = $this->upload->data('file_name');
				}
			}

			if ($this->admin_model->update_page($data)) {
				$this->session->set_flashdata('pages_message', $this->lang->line('sayfa_guncellendi'));

				redirect(site_url('admin/pages'));
			} else {
				$this->session->set_flashdata('pages_message', $this->lang->line('sayfa_guncelleme_hatasi'));

				redirect(site_url('admin/pages'));
			}
		} else {
			redirect(site_url('404'));
		}
	}
}