<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require './vendor/twitter/autoload.php';
require './vendor/facebook/src/Facebook/autoload.php';

use Abraham\TwitterOAuth\TwitterOAuth;

class Home extends MY_Controller {
	public function __construct() {
		parent::__construct();

		if ($this->session->userdata('id') == '') {
			redirect(site_url('404'));
		}
	}

	public function index() {
		if (isset($_GET['code'])) {
			redirect(site_url('locker/get_ig/?code=') . $_GET['code']);
		}

		if ($this->session->userdata('tw_token') != '') {
			define('CONSUMER_KEY', 'NfGQYXR6B8Sv7e6STYt24YtPe');
			define('CONSUMER_SECRET', 'kgEIAgPBbs1JrRWcuvNrK7cXEmZXuf3nH1STnR4qW1c1uZ99LO');
			define('OAUTH_CALLBACK', 'https://sellthing.co');

			$request_token = [];
			$request_token['oauth_token'] = $this->session->userdata('tw_token');
			$request_token['oauth_token_secret'] = $this->session->userdata('tw_secret_token');

			if (isset($_REQUEST['oauth_token']) && $request_token['oauth_token'] !== $_REQUEST['oauth_token']) {
				redirect(site_url('404'));
			} else {
				$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
				$access_token = $connection->oauth("oauth/access_token", ["oauth_verifier" => $_REQUEST['oauth_verifier']]);

				$this->session->set_userdata('tw_access_token', $access_token);

				redirect(site_url('locker/get_tw'));
			}
		}

		$data['title'] = $this->lang->line('home');

		$this->load->view('home/index', $data);
	}
}
