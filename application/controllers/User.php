<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends MY_Controller {
    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('id') == '') {
            redirect(site_url('404'));
        }
        
        $this->load->model('user_model');
    }

    public function index() {
        $data = array(
            'title' => $this->lang->line('hesap_ayarlari'),
            'settings' => $this->user_model->get_settings($this->session->userdata('id'))
        );

        $this->load->view('user/settings', $data);
    }

    public function change_password() {
        if (isset($_POST['passChange'])) {
            $data = array(
                'current_password' => $this->input->post('current_password'),
                'new_password' => $this->input->post('new_password'),
                'confirm_new_password' => $this->input->post('re_new_password'),
                'user_id' => $this->session->userdata('id')
            );

            if ($data['new_password'] != $data['confirm_new_password']) {
                $this->session->set_flashdata('settings_message', $this->lang->line('sifreler_uyusmuyor'));
                redirect(site_url('user/settings'));
            } else {
                if (strlen($data['new_password']) < 8) {
                    $this->session->set_flashdata('settings_message', $this->lang->line('en_az_sekiz_karakter'));
                    redirect(site_url('user/settings'));
                } else {
                    if ($this->user_model->change_password($data)) {
                        $this->session->set_flashdata('settings_message', $this->lang->line('sifre_degistirme_tamam_2'));
                        redirect(site_url('user/settings'));
                    } else {
                        redirect(site_url('user/settings'));
                    }
                }
            }
        } else {
            show_404();
        }
    }

    public function change_general() {
        if (isset($_POST['generalChange'])) {
            if ($this->input->post('new_mail') != '' && $this->input->post('password_mail')) {
                $data = array(
                    'new_mail' => $this->input->post('new_mail'),
                    'password_mail' => $this->input->post('password_mail'),
                    'code' => random_str(30)
                );
                
                if (!valid_email($data['new_mail'])){
                    $this->session->set_flashdata('settings_message', $this->lang->line('eposta_gecersiz'));

                    redirect(site_url('user/settings'));
                }
            }

            if (!empty($_FILES['changePicture']['name'])) {
				$config = array(
					'upload_path' => './uploads/user-img/',
					'allowed_types' => 'jpg|jpeg|png|gif',
					'file_name' => random_str()
				);
				
				$this->load->library('upload', $config);

				if (!$this->upload->do_upload('changePicture')) {
                    $this->session->set_flashdata('settings_message', $this->lang->line('profil_resmi_yukleme_hatasi'));
                    
					redirect(site_url('user/settings'));
				} else {
					$data['profile_picture'] = $this->upload->data('file_name');
				}
            }
            
            $data['user_id'] = $this->session->userdata('id');
            
            if ($this->user_model->change_general($data)) {
                if ($data['new_mail'] == '') {
                    $this->session->set_flashdata('settings_message', $this->lang->line('ayarlar_guncellendi'));
                } else {
                    $this->session->set_flashdata('settings_message', $this->lang->line('ayarlar_guncellendi_2')); 
                }

                redirect(site_url('user/settings'));
            } else {
                redirect(site_url('user/settings'));
            }
        }
    }
}
