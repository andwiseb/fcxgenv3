<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

require_once './vendor/autoload.php';
require './vendor/instagram/vendor/autoload.php';
require './vendor/twitter/autoload.php';

use EspressoDev\InstagramBasicDisplay\InstagramBasicDisplay;
use Phpfastcache\Helper\Psr16Adapter;
use Abraham\TwitterOAuth\TwitterOAuth;

class Locker extends MY_Controller {
	public function __construct() {
        parent::__construct();

        if ($this->session->userdata('id') == '') {
			redirect(site_url('404'));
		}

		$this->load->model('locker_model');
		

		define('CONSUMER_KEY', 'NfGQYXR6B8Sv7e6STYt24YtPe');
		define('CONSUMER_SECRET', 'kgEIAgPBbs1JrRWcuvNrK7cXEmZXuf3nH1STnR4qW1c1uZ99LO');
		define('OAUTH_CALLBACK', 'https://sellthing.co');
    }

	public function get_ig() {
		if ($this->session->userdata('ig_username') == '') {
			$instagram = new InstagramBasicDisplay([
				'appId' => '350488329564190',
				'appSecret' => '62f833c225e184335064247bc9c18093',
				'redirectUri' => 'https://sellthing.co/'
			]);

			if (isset($_GET['code'])) {
				// INSTAGRAM BASIC
				$code = $_GET['code'];
				$token = $instagram->getOAuthToken($code, true);
				$instagram->setAccessToken($token);
				$profile = $instagram->getUserProfile();

				// SCRAPER
				$instagram_login = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), 'sellthingco', 'Aslaolmaz1234.', new Psr16Adapter('Files'));
				$account = $instagram_login->getAccount($profile->username);

				$this->session->set_userdata('ig_count', $account->getFollowsCount());
				$this->session->set_userdata('ig_username', $profile->username);
				
				redirect('https://instagram.com/sellthingco');
			} else {
				$data['link'] = $instagram->getLoginUrl();

				echo json_encode($data);
			}
		} else {
			$data['link'] = 'https://instagram.com/sellthingco';

			echo json_encode($data);
		}
	}

	public function confirm_ig() {
		if ($this->session->userdata('ig_username') != '') {
			$instagram_login = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), 'sellthingco', 'Aslaolmaz1234.', new Psr16Adapter('Files'));
			$account = $instagram_login->getAccount($this->session->userdata('ig_username'));

			if ($account->getFollowsCount() > $this->session->userdata('ig_count')) {
				if ($this->locker_model->confirm_ig($this->session->userdata('ig_username'))) {
					$this->session->unset_userdata('ig_count');
					$this->session->unset_userdata('ig_username');

					$data['result'] = true;
				}
			} else {
				$data['result'] = false;
			}

			echo json_encode($data);
		} else {
			redirect(site_url('404'));
		}
	}

	public function get_tw() {
		if ($this->session->userdata('tw_access_token') == '') {
			$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
			$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));
			
			$this->session->set_userdata('tw_token', $request_token['oauth_token']);
			$this->session->set_userdata('tw_secret_token', $request_token['oauth_token_secret']);

			$data['link'] = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));

			echo json_encode($data);
		}
	}

	public function confirm_tw() {
		if ($this->session->userdata('tw_access_token') != '') {
			if ($this->session->userdata('count') == '') {
				$this->session->set_userdata('count', 1);
			}

			$access_token = $this->session->userdata('tw_access_token');
			$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
			$tweet = 'deneme';
			$tweeting = $connection->post("statuses/update", ["status" => $tweet]);

			if ($this->session->userdata('count') == 1) {
				if ($connection->getLastHttpCode() == 200) {
					$this->session->unset_userdata('tw_token');
					$this->session->unset_userdata('tw_secret_token');
					$this->session->set_userdata('count', 2);
				}
			}

			if ($this->locker_model->confirm_tw()) {
				$data['result'] = true;
			} else {
				$data['result'] = false;
			}

			echo json_encode($data);
		}
	}

	public function get_fb() {
		$this->session->set_userdata('code', random_str());

		$config = array(
			'app_id' => '295498201448905',
			'link' => base_url(),
			'redirect_uri' => site_url('locker/confirm_fb?code=') . $this->session->userdata('code')
		);

		$data['link'] = 'https://www.facebook.com/dialog/feed?app_id=' . $config['app_id'] . '&link=' . $config['link'] . '&redirect_uri=' . $config['redirect_uri'];

		echo json_encode($data);
	}

	public function confirm_fb() {
		if ($this->session->userdata('code') != '') {
			if ($this->locker_model->confirm_fb()) {
				$this->session->unset_userdata('code');

				echo "<script>window.close();</script>";
			} else {
				redirect(site_url('404'));
			}
		} else {
			redirect(site_url('404'));
		}
	}

	public function get_yt() {
		$OAUTH2_CLIENT_ID = '264381455082-n3kgsr03pa4t0dkcpt34l2r0fb389svq.apps.googleusercontent.com';
		$OAUTH2_CLIENT_SECRET = 'rKWmue6O9Ex_vQtloOW_aVXu';
		$client = new Google_Client();

		$client->setClientId($OAUTH2_CLIENT_ID);
		$client->setClientSecret($OAUTH2_CLIENT_SECRET);
		$client->setScopes('https://www.googleapis.com/auth/youtube');

		$redirect = filter_var(site_url('locker/get_yt'), FILTER_SANITIZE_URL);

		$client->setRedirectUri($redirect);

		$youtube = new Google_Service_YouTube($client);
		$tokenSessionKey = 'token-' . $client->prepareScopes();

		if (isset($_GET['code'])) {
			if (strval($this->session->userdata('state')) !== strval($_GET['state'])) {
				die('The session state did not match.');
			}
		  
			$client->authenticate($_GET['code']);

			$this->session->set_userdata($tokenSessionKey, $client->getAccessToken());

			header('Location: ' . $redirect);
		}

		if (isset($_SESSION[$tokenSessionKey])) {
			$client->setAccessToken($_SESSION[$tokenSessionKey]);
		}

		if ($client->getAccessToken()) {
			try {
				$resourceId = new Google_Service_YouTube_ResourceId();

				$resourceId->setChannelId('UCtVd0c0tGXuTSbU5d8cSBUg');
				$resourceId->setKind('youtube#channel');

				$subscriptionSnippet = new Google_Service_YouTube_SubscriptionSnippet();

				$subscriptionSnippet->setResourceId($resourceId);
			
				$subscription = new Google_Service_YouTube_Subscription();

				$subscription->setSnippet($subscriptionSnippet);

				$subscriptionResponse = $youtube->subscriptions->insert('id,snippet', $subscription, array());

				if ($this->locker_model->confirm_yt()) {
					$this->session->unset_userdata($tokenSessionKey);
					$this->session->unset_userdata('state');
	
					echo "<script>window.close();</script>";
				} else {
					redirect(site_url('404'));
				}
			} catch (Google_Service_Exception $e) {
				echo sprintf('<p>A service error occurred: <code>%s</code></p>', htmlspecialchars($e->getMessage()));
				echo "<script>window.close();</script>";
			} catch (Google_Exception $e) {
				echo sprintf('<p>A</p>', htmlspecialchars($e->getMessage()));
			}

			$this->session->set_userdata($tokenSessionKey, $client->getAccessToken());
		} else {
			$state = mt_rand();

			$client->setState($state);

			$this->session->set_userdata('state', $state);

			$authUrl = $client->createAuthUrl();

			$data['link']  = $authUrl;

			echo json_encode($data);
		}
	}
}
