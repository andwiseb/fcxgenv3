<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounts extends MY_Controller {
	public function __construct() {
        parent::__construct();

        if ($this->session->userdata('id') == '') {
			redirect(site_url('404'));
		} else {
            $this->load->model('accounts_model');
            
        }
    }

	public function index() {
        $data = array(
            'title' => $this->lang->line('hesap_olusturucu'),
            'categories' => $this->accounts_model->get_categories()
        );

		$this->load->view('accounts/index', $data);
    }
    
    public function generate($language, $url) {
        if (id_user_info($this->session->userdata('id'))->share == 0) {
            $data = array(
                'title' => $this->lang->line('paylasim_kilidi')
            );

            $this->load->view('accounts/locker', $data);
        } else {
            if ($this->session->flashdata('refresh') != '') {
                redirect(site_url('generator'));
            } else {
                if ($this->session->userdata('ban') == '') {
                    $this->session->set_userdata('ban', 0);
                } else if ($this->session->userdata('ban') == 3) {
                    $this->session->unset_userdata('ban');
                    $this->session->set_tempdata('ban', '4', 300);
                }

                if (!$this->session->tempdata('ban')) {
                    $result = $this->accounts_model->generate($url);

                    if ($result) {
                        $data = array(
                            'title' => $this->lang->line('sonuc'),
                            'code' => $result->code,
                            'email' => $result->email,
                            'password' => $result->password,
                            'caption' => $result->caption
                        );

                        //$this->session->set_flashdata('refresh', '1');
                        $this->session->set_userdata('ban', $this->session->userdata('ban') + 1);
                        $this->load->view('accounts/result', $data);
                    } else {
                        $this->session->set_userdata('ban', $this->session->userdata('ban') - 1);
                        $this->session->set_flashdata('error', $this->lang->line('hesap_bulunamadi'));

                        redirect(site_url('generator'));
                    }
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('sureli_ban'));

                    redirect(site_url('generator'));
                }
            }
        }
	}
}
