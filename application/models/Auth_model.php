<?php

class Auth_model extends CI_Model {
	public function __construct() {
        parent::__construct();

        date_default_timezone_set('Europe/Istanbul');
    }

    // HELPER EPOSTA VARMI KONTROLÜ
    public function email_control($email) {
        $this->db->where('email', $email);

        if ($this->db->get('members')->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    // KAYIT
    public function register($data) {
        $this->db->set('email', $data['email']);
        $this->db->set('password', $data['password']);
        $this->db->set('create_date', date("Y-m-d H:i:s"));
        $this->db->insert('members');

        $member_id = $this->db->insert_id();

        $this->db->set('member_id', $member_id);
        $this->db->insert('shares');

        $this->db->set('code', $data['code']);
        $this->db->set('type_id', 1);
        $this->db->set('member_id', $member_id);

        return $this->db->insert('requests');
    }
    
    // GİRİŞ
    public function login($data) {
        $this->db->where('email', $data['email']);

        $result = $this->db->get('members');

        if ($result->num_rows() > 0) {
            if ($result->row()->verify == 0) {
                $this->session->set_flashdata('login_message', $this->lang->line('aktive_edilmemis'));

                return false;
            } else {
                if (password_verify($data['password'], $result->row()->password)) {
                    $this->db->set('last_login', date('Y-m-d H:i:s'));
                    $this->db->where('email', $data['email']);
                    
                    return $this->db->update('members');
                } else {
                    $this->session->set_flashdata('login_message', $this->lang->line('eposta_sifre_hatali'));

                    return false;
                }
            }
        } else {
            $this->session->set_flashdata('login_message', $this->lang->line('eposta_sifre_hatali_2'));

            return false;
        }
    }

    // HELPER EMAIL İLE KULLANICI BİLGİLERİ
    public function email_user_info($email) {
        $this->db->where('email', $email);

        return $this->db->get('members')->row();
    }

    // HELPER KULLANICI ID İLE KULLANICI BİLGİLERİ
    public function id_user_info($id) {
        $this->db->select('*');
        $this->db->from('members');
        $this->db->join('shares', 'shares.member_id = members.id');
        $this->db->where('members.id', $id);

        $result = $this->db->get();

        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    // ÜYELİK AKTİVASYON
    public function verify($language, $code) {
        $this->db->where('code', $code);

        $result = $this->db->get('requests');

        if ($result->num_rows() > 0) {
            $this->db->set('verify', 1);
            $this->db->where('id', $result->row()->member_id);

            if ($this->db->update('members')) {
                $this->db->where('code', $code);

                return $this->db->delete('requests');
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function recovery($data) {
        $this->db->where('email', $data['email']);

        $member_result = $this->db->get('members');

        $this->db->where('member_id', $member_result->row()->id);
        $this->db->where('type_id', 2);

        $request_result = $this->db->get('requests');

        if ($request_result->num_rows() > 0) {
            $this->db->where('member_id', $member_result->row()->id);
            $this->db->where('type_id', 2);
            $this->db->delete('requests');
        }

        $this->db->set('code', $data['code']);
        $this->db->set('type_id', 2);
        $this->db->set('member_id', $member_result->row()->id);

        return $this->db->insert('requests');
    }

    public function change_password($data) {
        if ($this->session->flashdata('change_password') != '') {
            $this->db->where('code', $data['code']);

            $result = $this->db->get('requests');

            $this->db->set('password', password_hash($data['password'], PASSWORD_DEFAULT));
            $this->db->where('id', $result->row()->member_id);

            if ($this->db->update('members')) {
                $this->db->where('code', $data['code']);
                $this->db->where('type_id', 2);

                return $this->db->delete('requests');
            } else {
                return false;
            }
        } else {
            $this->db->where('code', $data['code']);
            $this->db->where('type_id', 2);

            $result = $this->db->get('requests');

            if ($result->num_rows() > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function change_email($code) {
        $this->db->where('code', $code);

        $result = $this->db->get('requests');

        if ($result->num_rows() > 0) {
            $this->db->set('verify', 1);
            $this->db->set('email', $result->row()->email);
            $this->db->where('id', $result->row()->member_id);

            if ($this->db->update('members')) {
                $this->db->where('code', $code);

                return $this->db->delete('requests');
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
