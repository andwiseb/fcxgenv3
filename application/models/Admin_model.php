<?php

class Admin_model extends CI_Model {
	public function __construct() {
        parent::__construct();

        date_default_timezone_set('Europe/Istanbul');
    }

    public function get_settings() {
        return $this->db->get('settings')->row();
    }

    public function update_settings($data) {
        $this->db->where('id', 1);
        
        if ($this->db->update('settings', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function get_categories() {
        return $this->db->get('categories');
    }

    public function add_category($data) {
        $this->db->set('title', $data['meta_title']);
        $this->db->set('description', $data['meta_description']);
        $this->db->set('keywords', $data['meta_keywords']);
        $this->db->insert('meta_tags');

        $meta_id = $this->db->insert_id();

        $this->db->where('url', $data['url']);

        $category = $this->db->get('categories');

        if ($category->num_rows() > 0) {
            $this->session->set_flashdata('categories_message', $this->lang->line('kategori_var_hatasi'));

            return false;
        } else {
            $this->db->set('title', $data['title']);
            $this->db->set('url', $data['url']);
            $this->db->set('meta_id', $meta_id);
            $this->db->insert('categories');
    
            $this->db->set('url', 'category/' . $data['url']);
            $this->db->set('meta_id', $meta_id);
            $this->db->set('type_id', 1);
            
            if ($this->db->insert('pages')) {
                return true;
            } else {
                $this->session->set_flashdata('categories_message', $this->lang->line('kategori_ekleme_hatasi'));

                return false;
            }
        }
    }

    public function delete_category($id) {
        $this->db->where('id', $id);
            
        $category = $this->db->get('categories');

		$this->db->where('id', $id);
        $this->db->delete('categories');

        $this->db->where('url', 'category/' . $category->row()->url);
        $this->db->delete('pages');
        
        $this->db->where('id', $category->row()->meta_id);

        if ($this->db->delete('meta_tags')) {
            $this->session->set_flashdata('categories_message', $this->lang->line('kategori_silindi'));

            return true;
        } else {
            return false;
        }
    }
    
    public function edit_category($id) {
        $this->db->select('*, categories.id as c_id, categories.title as c_title, meta_tags.title as m_title');
        $this->db->from('categories');
        $this->db->join('meta_tags', 'categories.meta_id = meta_tags.id');
        $this->db->where('categories.id', $id);
            
        return $this->db->get()->row();
    }
    
    public function update_category($data) {
        $this->db->where('id', $data['id']);

        $category = $this->db->get('categories');

        $this->db->set('url', 'category/' . $data['url']);
        $this->db->where('url', 'category/' . $category->row()->url);
        $this->db->update('pages');

        $this->db->set('title', $data['meta_title']);
        $this->db->set('description', $data['meta_description']);
        $this->db->set('keywords', $data['meta_keywords']);
        $this->db->where('id', $category->row()->meta_id);
        $this->db->update('meta_tags');
        
        $this->db->set('title', $data['title']);
        $this->db->set('url', $data['url']);
        $this->db->where('id', $data['id']);

        if ($this->db->update('categories')) {
            return true;
        } else {
            return false;
        }
    }

    public function get_platforms() {
        return $this->db->get('platforms');
    }

    public function add_platform($data) {
        $this->db->set('title', $data['meta_title']);
        $this->db->set('description', $data['meta_description']);
        $this->db->set('keywords', $data['meta_keywords']);
        $this->db->set('image', $data['image']);
        $this->db->insert('meta_tags');

        $meta_id = $this->db->insert_id();

        $this->db->where('url', $data['url']);

        $platform = $this->db->get('platforms');

        if ($platform->num_rows() > 0) {
            $this->session->set_flashdata('platforms_message', $this->lang->line('platform_var_hatasi'));

            return false;
        } else {
            $this->db->set('title', $data['title']);
            $this->db->set('category_id', $data['category_id']);
            $this->db->set('url', $data['url']);
            $this->db->set('image', $data['image']);
            $this->db->set('meta_id', $meta_id);
            $this->db->insert('platforms');
    
            $this->db->set('url', 'platform/' . $data['url']);
            $this->db->set('meta_id', $meta_id);
            $this->db->set('type_id', 1);
    
            if ($this->db->insert('pages')){
                return true;
            } else {
                $this->session->set_flashdata('platforms_message', $this->lang->line('platform_ekleme_hatasi'));

                return false;
            }
        }
    }

    public function delete_platform($id) {
        $this->db->where('id', $id);
            
        $platform = $this->db->get('platforms');

		$this->db->where('id', $id);
        $this->db->delete('platforms');

        $this->db->where('url', 'platform/' . $platform->row()->url);
        $this->db->delete('pages');

        $this->db->where('id', $platform->row()->meta_id);

        if ($this->db->delete('meta_tags')) {
            unlink(FCPATH . 'resources/assets/img/' . $platform->row()->image);

            $this->session->set_flashdata('platform_message', $this->lang->line('platform_silindi'));

            return true;
        } else {
            return false;
        }
    }

    public function edit_platform($id) {
        $this->db->select('*, platforms.id as p_id, platforms.title as p_title, meta_tags.title as m_title');
        $this->db->from('platforms');
        $this->db->join('meta_tags', 'platforms.meta_id = meta_tags.id');
        $this->db->where('platforms.id', $id);
            
        return $this->db->get()->row();
    }

    public function update_platform($data) {
        $this->db->where('id', $data['id']);

        $platform = $this->db->get('platforms');

        $this->db->set('url', 'platform/' . $data['url']);
        $this->db->where('url', 'platform/' . $platform->row()->url);
        $this->db->update('pages');

        $this->db->set('title', $data['title']);
        $this->db->set('url', $data['url']);
        $this->db->set('category_id', $data['category_id']);

        if (isset($data['image'])) {
            $this->db->set('image', $data['image']);

            unlink(FCPATH . 'resources/assets/img/' . $platform->row()->image);
        }

        $this->db->where('id', $data['id']);
        $this->db->update('platforms');

        $this->db->set('title', $data['meta_title']);
        $this->db->set('description', $data['meta_description']);
        $this->db->set('keywords', $data['meta_keywords']);
        $this->db->where('id', $platform->row()->meta_id);

        if ($this->db->update('meta_tags')) {
            return true;
        } else {
            return false;
        }
    }

    public function get_accounts() {
        $this->db->select('*, platforms.title as p_title, accounts.password as a_password, accounts.email as a_email, account_types.title as at_title');
        $this->db->from('accounts');
        $this->db->join('platforms', 'platforms.id = accounts.platform_id');
        $this->db->join('members', 'members.id = accounts.member_id');
        $this->db->join('account_types', 'account_types.id = accounts.type_id');
        $this->db->order_by('accounts.upload_time', 'asc');


        return $this->db->get();
    }

    public function single_add_account($data) {
        $explode = explode(":", $data['mail_pass']);

        $this->db->set('platform_id', $data['platform_id']);
        $this->db->set('email', $explode[0]);
        $this->db->set('password', $explode[1]);
        $this->db->set('caption', $data['caption']);
        $this->db->set('member_id', $data['member_id']);
        $this->db->set('type_id', $data['type_id']);
        $this->db->set('code', $data['code']);
        $this->db->set('date', $data['date']);

        return $this->db->insert('accounts');
    }

    public function bulk_add_account($data) {
        $this->db->set('platform_id', $data['platform_id']);
        $this->db->set('email', $data['email']);
        $this->db->set('password', $data['password']);
        $this->db->set('caption', $data['caption']);
        $this->db->set('member_id', $data['member_id']);
        $this->db->set('type_id', $data['type_id']);
        $this->db->set('date', $data['date']);
        $this->db->set('code', random_str(15));

        return $this->db->insert('accounts');
    }

    public function get_pages() {
        $this->db->select('*, pages.id as p_id, meta_tags.title as m_title, page_types.title as t_title');
        $this->db->from('pages');
        $this->db->join('meta_tags', 'pages.meta_id = meta_tags.id');
        $this->db->join('page_types', 'pages.type_id = page_types.id');
        
        return $this->db->get();
    }
    
    public function delete_page($id) {
        $this->db->where('id', $id);
            
        $page = $this->db->get('pages');

		$this->db->where('id', $id);
        $this->db->delete('pages');

        $this->db->where('id', $page->row()->meta_id);

        $meta_tag = $this->db->get('meta_tags');

        $this->db->where('id', $page->row()->meta_id);

        if ($this->db->delete('meta_tags')) {
            if ($meta_tag->row()->image != '') {
                unlink(FCPATH . 'resources/assets/img/' . $meta_tag->row()->image);
            }

            $this->session->set_flashdata('page_message', $this->lang->line('sayfa_silindi'));

            return true;
        } else {
            return false;
        }
    }

    public function add_page($data) {
        $this->db->set('title', $data['meta_title']);
        $this->db->set('description', $data['meta_description']);
        $this->db->set('keywords', $data['meta_keywords']);

        if (isset($data['meta_image'])) {
            $this->db->set('image', $data['meta_image']);  
        }

        $this->db->insert('meta_tags');

        $meta_id = $this->db->insert_id();

        $this->db->where('url', $data['url']);

        $page = $this->db->get('pages');

        if ($page->num_rows() > 0) {
            $data['url'] = $data['url'] . '-' . date('Y-m-d') . '-' . rand(1, 99);
        }

        $this->db->set('url', $data['url']);
        $this->db->set('meta_id', $meta_id);
        $this->db->set('type_id', 1);
        
        if ($this->db->insert('pages')) {
            return true;
        } else {
            return false;
        }
    }

    public function edit_page($id) {
        $this->db->select('*, pages.id as p_id');
        $this->db->from('pages');
        $this->db->join('meta_tags', 'pages.meta_id = meta_tags.id');
        $this->db->where('pages.id', $id);
            
        return $this->db->get()->row();
    }

    public function update_page($data) {
        $this->db->where('id', $data['id']);

        $page = $this->db->get('pages');

        $this->db->set('url', $data['url']);
        $this->db->where('id', $data['id']);
        $this->db->update('pages');

        $this->db->where('id', $page->row()->meta_id);

        $meta_tag = $this->db->get('meta_tags');

        $this->db->set('title', $data['meta_title']);
        $this->db->set('description', $data['meta_description']);
        $this->db->set('keywords', $data['meta_keywords']);

        if (isset($data['meta_image'])) {
            $this->db->set('image', $data['meta_image']);

            unlink(FCPATH . 'resources/assets/img/' . $meta_tag->row()->image);
        }

        $this->db->where('id', $page->row()->meta_id);

        if ($this->db->update('meta_tags')) {
            return true;
        } else {
            return false;
        }
    }
}
