<?php

class Categories_model extends CI_Model {
	public function __construct() {
        parent::__construct();
    }

    public function get_categories($url) {
        $this->db->where('url', $url);

        $result = $this->db->get('categories');

        $this->db->where('category_id', $result->row()->id);
        
        return $this->db->get('platforms');
    }

    public function get_title($url) {
        $this->db->where('url', $url);
        
        return $this->db->get('categories')->row()->title;
    }
}
