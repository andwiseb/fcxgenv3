<?php

class User_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->helper('email');
    }

    public function get_settings($id) {
        $this->db->where('id', $id);

        return $this->db->get('members')->row();
    }

    public function change_password($data) {
        $this->db->where('id', $data['user_id']);

        $result = $this->db->get('members')->row();

        if (password_verify($data['current_password'], $result->password)) {
            if (password_verify($data['new_password'], $result->password)) {
                $this->session->set_flashdata('settings_message', $this->lang->line('eski_sifre_ayni'));
                
                return false;
            } else {
                $this->db->set('password', password_hash($data['new_password'], PASSWORD_DEFAULT));
                $this->db->where('id', $data['user_id']);
                
                return $this->db->update('members');
            }
           
        } else {
            $this->session->set_flashdata('settings_message', $this->lang->line('confirm_new_password'));
            
            return false;
        }
    }


    public function change_general($data) {
        $this->db->where('id', $data['user_id']);

        $result = $this->db->get('members')->row();
        
        if ($data['password_mail'] != ''){
            if (password_verify($data['password_mail'], $result->password)) {
                if ($data['new_mail'] != $result->email) {
                    $this->db->where('member_id', $this->session->userdata('id'));
                    $this->db->where('type_id', 3);
                    $this->db->delete('requests');

                    $this->db->set('code', $data['code']);
                    $this->db->set('type_id', 3);
                    $this->db->set('member_id', $this->session->userdata('id'));
                    $this->db->set('email', $data['new_mail']);
                    $this->db->insert('requests');

                    $change_email_url = site_url('change-email/') . $data['code'];

                    send_mail($data['new_mail'], $change_email_url, $this->lang->line('eposta_degisikligi'));

                    if ($data['profile_picture'] == '') {
                        return true;
                    }
                } else {
                    $this->session->set_flashdata('settings_message', $this->lang->line('eski_eposta_ayni'));
                    
                    return false;
                }
            } else {
                $this->session->set_flashdata('settings_message', $this->lang->line('sifre_hatali'));
                
                return false;
            }
        }

        if ($data['profile_picture'] != ''){
            $this->db->set('img', $data['profile_picture']);
            $this->db->where('id', $data['user_id']);

            return $this->db->update('members');
        }
    }
}
