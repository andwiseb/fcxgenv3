<?php

class Platforms_model extends CI_Model {
	public function __construct() {
        parent::__construct();
    }

    public function get_platforms($url) {
        $this->db->where('url', $url);
        
        return $this->db->get('platforms')->row();
    }

    public function get_title($url) {
        $this->db->where('url', $url);
        
        return $this->db->get('platforms')->row()->title;
    }
}
