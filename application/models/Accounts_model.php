<?php

class Accounts_model extends CI_Model {
    public function __construct() {
        parent::__construct();

        date_default_timezone_set('Europe/Istanbul');
    }

    public function get_categories() {
        $categories = $this->db->get('categories');
        $count = 0;
        $data = array();

        foreach ($categories->result() as $category) {
            $this->db->where('category_id', $category->id);

            $result = $this->db->get('platforms');

            if ($result->num_rows() > 0) {
                $std = new stdClass();

                $std->id = $category->id;
                $std->title = $category->title;
                $std->url = $category->url;
                $std->meta_title = $category->meta_title;
                $std->meta_description = $category->meta_description;
                $std->meta_keywords = $category->meta_keywords;

                $data[$count] = $std;
            }

            $count++;
        }
        
        return $data;
    }

    public function get_platforms_with_category($category_id) {
        $this->db->where('category_id', $category_id);

        return $this->db->get('platforms');
    }

    public function generate($url) {
        $this->db->where('url', $url);

        $platform = $this->db->get('platforms');

        $this->db->where('platform_id', $platform->row()->id);

        $exist = $this->db->get('accounts');

        if ($exist->num_rows() > 0) {
            $this->db->where('platform_id', $platform->row()->id);
            $this->db->order_by('rand()');
            $this->db->limit(1);

            $account = $this->db->get('accounts');

            //$this->db->where('id', $account->row()->id);
            //$this->db->delete('accounts');

            $this->db->set('date', date('Y-m-d'));
            $this->db->set('time', date('H:i:s'));
            $this->db->set('account_id', $account->row()->id);
            $this->db->set('member_id', $this->session->userdata('id'));
            $this->db->insert('generator_logs');

            return $account->row();
        } else {
            return false;
        }
    }
}
