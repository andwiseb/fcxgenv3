<?php

class Misc_model extends CI_Model {
	public function __construct() {
        parent::__construct();
    }

    public function subscribe($data) {
        $this->db->set('email', $data);
        
        return $this->db->insert('subscribes');
    }

    public function reset_share() {
        $this->db->set('share', 0);
        
        return $this->db->update('shares');
    }
    
    public function sitemap() {
        return $this->db->get('pages');
	}
    
    public function ig_control($list) {
        $shares = $this->db->get('shares');

        foreach ($shares->result() as $share) {
            for ($i = 0; $i <= count($list['accounts']); $i++) {
                if (!in_array($share->ig_username, $list)) {
                    $this->db->set('ig_username', NULL);
                    $this->db->set('share', 0);
                    $this->db->where('id', $share->id);
                    $this->db->update('shares');
                }
            }
        }

        return true;
    }
    
    public function get_meta_tag($url) {
        $this->db->select('*');
        $this->db->from('pages');
        $this->db->join('meta_tags', 'pages.meta_id = meta_tags.id');
        $this->db->where('pages.url', $url);
        
        return $this->db->get()->row();
	}
}
