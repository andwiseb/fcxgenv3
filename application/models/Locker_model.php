<?php

class Locker_model extends CI_Model {
	public function __construct() {
        parent::__construct();
    }

    public function confirm_ig($username) {
        date_default_timezone_set('Europe/Istanbul');

        $this->db->set('ig_username', $username);
        $this->db->set('share', 1);
        $this->db->set('last_share_date', date('Y-m-d H:i:s'));
        $this->db->where('member_id', $this->session->userdata('id'));

        return $this->db->update('shares');
    }

    public function confirm_tw() {
        date_default_timezone_set('Europe/Istanbul');

        $this->db->set('share', 1);
        $this->db->set('last_share_date', date('Y-m-d H:i:s'));
        $this->db->where('member_id', $this->session->userdata('id'));

        return $this->db->update('shares');
    }

    public function confirm_fb() {
        date_default_timezone_set('Europe/Istanbul');

        $this->db->set('share', 1);
        $this->db->set('last_share_date', date('Y-m-d H:i:s'));
        $this->db->where('member_id', $this->session->userdata('id'));

        return $this->db->update('shares');
    }

    public function confirm_yt() {
        date_default_timezone_set('Europe/Istanbul');

        $this->db->set('share', 1);
        $this->db->set('last_share_date', date('Y-m-d H:i:s'));
        $this->db->where('member_id', $this->session->userdata('id'));

        return $this->db->update('shares');
    }
}
