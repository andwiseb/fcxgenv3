<?php

$lang['hesap_olusturucu'] = "Générateur de compte";
$lang['paylasim_kilidi'] = "Partager le verrouillage";
$lang['sonuc'] = "Résultat";
$lang['hesap_bulunamadi'] = "Compte non trouvé. Veuillez réessayer plus tard.";
$lang['sureli_ban'] = "Vous avez été banni pendant une courte période. S'il vous plait, réessayez dans quelques minutes.";
$lang['logo_yuklenme_hatasi'] = "Une erreur s'est produite lors du chargement du logo.";
$lang['favicon_yuklenme_hatasi'] = "Une erreur s'est produite lors du chargement du favicon.";
$lang['ayarlar_guncellendi'] = "Les paramètres ont été mis à jour.";
$lang['ayar_guncelleme_hatasi'] = "Une erreur s'est produite lors de la mise à jour des paramètres.";
$lang['yonetici_paneli'] = "Panneau d'administration";
$lang['kategoriler'] = "Catégories";
$lang['kategori_eklendi'] = "La catégorie a été ajoutée.";
$lang['kategori_ekleme_hatasi'] = "Une erreur s'est produite lors de l'ajout de la catégorie.";
$lang['kategori_guncellendi'] = "La catégorie a été mise à jour.";
$lang['kategori_guncelleme_hatasi'] = "Une erreur s'est produite lors de la mise à jour de la catégorie.";
$lang['platformlar'] = "Plateformes";
$lang['kategori_seciniz'] = "Le champ de catégorie ne peut pas être laissé vide.";
$lang['resim_yukleme_hatasi'] = "Une erreur s'est produite lors du téléchargement de l'image.";
$lang['platform_eklendi'] = "La plateforme a été ajoutée.";
$lang['platform_ekleme_hatasi'] = "Une erreur s'est produite lors de l'ajout de la plateforme.";
$lang['platform_guncellendi'] = "La plateforme a été mise à jour.";
$lang['platform_guncelleme_hatasi'] = "Une erreur s'est produite lors de la mise à jour de la plate-forme.";
$lang['iki_nokta_ust_uste_hatasi'] = "Veuillez mettre deux points (:) entre l'e-mail et le mot de passe.";
$lang['platform_seciniz'] = "La zone de la plate-forme ne peut pas être laissée vide.";
$lang['hesap_ekleme_hatasi'] = "Une erreur s'est produite lors de l'ajout du compte.";
$lang['hesap_eklendi'] = "Le compte a été ajouté.";
$lang['hesaplar_eklendi'] = "Des comptes ont été ajoutés.";
$lang['home'] = "Page d'accueil";
$lang['kayit_ol'] = "S'inscrire";
$lang['en_az_sekiz_karakter'] = "Le mot de passe doit comporter au moins 8 caractères.";
$lang['üye_aktivasyonu'] = "Activation des membres";
$lang['kayit_tamam'] = "L'inscription est terminée. Veuillez vérifier votre adresse e-mail pour l'activation.";
$lang['kayit_hatasi'] = "Une erreur s'est produite lors de l'enregistrement.";
$lang['eposta_var'] = "Cette adresse e-mail est utilisée.";
$lang['sifreler_uyusmuyor'] = "Les mots de passe ne correspondent pas.";
$lang['giris_yap'] = "S'identifier";
$lang['hoscakal'] = "Au revoir.";
$lang['aktivasyon_tamam'] = "L'activation est terminée. Vous pouvez vous connecter.";
$lang['aktivasyon_hatasi'] = "Une erreur s'est produite lors de l'activation.";
$lang['sifre_sifirla'] = "Réinitialiser le mot de passe";
$lang['sifre_sifirlama_istegi'] = "Le lien de réinitialisation du mot de passe a été envoyé à votre adresse e-mail.";
$lang['sifre_sifirlama_hatasi'] = "Une erreur s'est produite lors de la réinitialisation du mot de passe.";
$lang['eposta_yok'] = "Cette adresse e-mail n'est pas utilisée.";
$lang['sifre_degistirme_tamam'] = "Le mot de passe a été changé. Vous pouvez vous connecter.";
$lang['sifre_degistirme_hatasi'] = "Une erreur s'est produite lors de la modification du mot de passe.";
$lang['gecersiz_istek'] = "Requête invalide.";
$lang['sifre_degistir'] = "Changer le mot de passe";
$lang['sifre_unuttum'] = 'Mot de passe oublié?';
$lang['hesaplar'] = "Comptes";
$lang['sayfa_bulunamadi'] = "Page non trouvée";
$lang['hesap_ayarlari'] = "Paramètres du compte";
$lang['sifre_degistirme_tamam_2'] = "Le mot de passe a été changé.";
$lang['eposta_gecersiz'] = "Email invalide.";
$lang['profil_resmi_yukleme_hatasi'] = "Une erreur s'est produite lors du chargement de la photo de profil.";
$lang['kategori_silindi'] = "La catégorie a été supprimée.";
$lang['platform_silindi'] = "La plateforme a été supprimée.";
$lang['aktive_edilmemis'] = "Veuillez activer votre compte.";
$lang['eposta_sifre_hatali'] = "E-mail ou mot de passe incorrect. <a href='" . site_url('recovery') . "'>Oublié mon mot de passe?</a>";
$lang['eposta_sifre_hatali_2'] = "E-mail ou mot de passe incorrect.";
$lang['eski_sifre_ayni'] = "Le nouveau mot de passe est le même que l'ancien mot de passe.";
$lang['eski_eposta_ayni'] = "Le nouvel e-mail est le même que l'ancien e-mail.";
$lang['sifre_hatali'] = "Le mot de passe est incorrect.";
$lang['olustur'] = "Produire";
$lang['platform_bulunamadi'] = "Plate-forme introuvable.";
$lang['kilit_aciklama'] = "Ce contenu est verrouillé. Pour déverrouiller, partager ou suivre le site en utilisant les boutons ci-dessous.";
$lang['paylas'] = "Partager";
$lang['takip_et'] = "Suivre";
$lang['eposta'] = "Email";
$lang['kullanici_adi'] = 'Nom d\'utilisateur';
$lang['sifre'] = "Mot de passe";
$lang['ayrintilar'] = "Détails";
$lang['liste'] = "Référencement";
$lang['tekli_ekle'] = "Ajouter un seul";
$lang['coklu_ekle'] = "Ajouter plusieurs";
$lang['kullanici'] = "Utilisateur";
$lang['tur'] = "Type";
$lang['kod'] = "Code";
$lang['hesap_bulunamadi'] = "Compte non trouvé.";
$lang['panele_geri_don'] = "Retour au panneau";
$lang['eposta_sifre'] = "Mot de passe de l'email";
$lang['sec'] = "Sélectionner";
$lang['ucretsiz'] = "Libre";
$lang['ucretli'] = "Payé";
$lang['duzenleme_iptali'] = "Annuler la modification";
$lang['kaydet'] = "sauvegarder";
$lang['dosya'] = "Fichier";
$lang['ekle_duzenle'] = "Ajouter / Modifier";
$lang['baslik'] = "Titre";
$lang['url'] = "URL";
$lang['eylem'] = "Action";
$lang['duzenle'] = "Éditer";
$lang['sil'] = "Supprimer";
$lang['kategori_bulunamadi'] = "Catégorie introuvable.";
$lang['meta_baslik'] = "Titre méta";
$lang['meta_aciklama'] = "Meta Description";
$lang['meta_anahtar_kelimeler'] = "Mots-clés méta";
$lang['kullanicilar'] = "Utilisateurs";
$lang['menuler'] = "Les menus";
$lang['sayfalar'] = "Des pages";
$lang['istatistikler'] = "Statistiques";
$lang['gorsel'] = "Image";
$lang['kategori'] = "Catégorie";
$lang['genel_ayarlar'] = "réglages généraux";
$lang['site_basligi'] = "Titre du site";
$lang['site_aciklamasi'] = "Description du site";
$lang['logo'] = "Logo";
$lang['favicon'] = "Favicon";
$lang['sifreyi_onayla'] = "Confirmez le mot de passe";
$lang['gonder'] = "Envoyer";
$lang['uye_degil_misin'] = "N'êtes-vous pas membre?";
$lang['uye_misin'] = "Déjà membre?";
$lang['profil_resmi_yukle'] = "Télécharger une photo de profil";
$lang['yeni_eposta'] = "Nouveau courriel";
$lang['mevcut_sifre'] = "Mot de passe actuel";
$lang['alan_doldurma_hatasi'] = "Merci de remplir tous les champs.";
$lang['yeni_sifre'] = "Nouveau mot de passe";
$lang['sifreyi_onayla_2'] = "Confirmez le mot de passe";
$lang['istediginiz_sayfa_yok'] = "La page que vous avez demandée n'a pas été trouvée!";
$lang['ana_sayfaya_don'] = "Retour à la page d'accueil";
$lang['ingilizce'] = "Anglais";
$lang['turkce'] = "Turc";
$lang['fransizca'] = "Français";
$lang['portekizce'] = "Portugais";
$lang['cikis_yap'] = "Déconnexion";
$lang['ayarlar'] = "Réglages";
$lang['abone_olma_tamam'] = 'Votre inscription à la newsletter a bien été prise en compte. Merci.';
$lang['abone_olma_hatasi'] = "Une erreur est survenue lors de l'inscription à la newsletter.";
$lang['cok_yakinda'] = 'Bientôt disponible';
$lang['cok_yakinda_aciklama'] = 'Nouveau design, nouvelles fonctionnalités et bien plus de comptes!';
$lang['gun'] = 'Journées';
$lang['saat'] = 'Heures';
$lang['dakika'] = 'Minutes';
$lang['saniye'] = 'Second';
$lang['bildirim_alin'] = "Abonnez-vous à la newsletter pour être averti de l'ouverture du site.";
$lang['abone_ol'] = 'Souscrire';
$lang['tum_haklari_saklidir'] = 'Tous les droits sont réservés';
$lang['cerez_kullanimi'] = 'Utilisation de cookies';
$lang['gizlilik_sozlesmesi'] = 'Accord de confidentialité';
$lang['kullanim_sartlari'] = "Conditions d'utilisation";
$lang['ve'] = 'et';
$lang['sifre_sifirlama_metin'] = 'Entrez votre email et les instructions vous seront envoyées!';
$lang['politika_gizlilik_kontrol'] = 'J\'accepte les <a href="' . site_url('recovery') . '">conditions d\'utilisation </a> et la <a href="' . site_url('recovery') . '">politique de confidentialité.</a>';
$lang['eposta_degistirin'] = "Veuillez changer votre adresse e-mail afin d'utiliser le site.";
$lang['ayarlar_guncellendi_2'] = "Les paramètres ont été mis à jour ... Pour terminer le processus de changement d'e-mail, veuillez cliquer sur le lien d'activation envoyé à votre adresse e-mail.";
$lang['eposta_degisikligi'] = 'Changement de courriel';
$lang['eposta_degistirme_tamam'] = "L'e-mail a été modifié.";
$lang['eposta_degistirme_hatasi'] = "Une erreur s'est produite lors de la modification de l'e-mail.";
$lang['sayfa_bulunamadi_2'] = 'Page non trouvée.';
$lang['meta_gorsel'] = 'Meta Image';
$lang['sayfa_eklendi'] = 'La page a été ajoutée.';
$lang['sayfa_ekleme_hatasi'] = "Une erreur s'est produite lors de l'ajout de la page.";
$lang['sayfa_guncellendi'] = 'La page a été mise à jour.';
$lang['sayfa_guncelleme_hatasi'] = "Une erreur s'est produite lors de la mise à jour de la page.";
$lang['sayfa_silindi'] = 'La page a été supprimée.';
$lang['kategori_var_hatasi'] = 'La catégorie existe déjà.';
$lang['platform_var_hatasi'] = 'La plateforme existe déjà.';
$lang['kategori_silme_hatasi'] = "Une erreur s'est produite lors de la suppression de la catégorie.";
$lang['platform_silme_hatasi'] = "Une erreur s'est produite lors de la suppression de la plate-forme.";
$lang['sayfa_silme_hatasi'] = "Une erreur s'est produite lors de la suppression de la page.";
$lang['bir_hata_olsutu'] = 'Un problème est survenu.';
$lang['platform'] = 'Plate-forme';
$lang['emin_misin'] = 'Êtes-vous sûr?';
$lang['geri_alinamaz'] = 'Cette action est irréversible!';
$lang['yuklenme_zamani'] = 'Heure de téléchargement';
//
$lang['meta_resim_yuklenme_hatasi'] = "Une erreur s'est produite lors du chargement de la méta-image.";