<?php $this->load->view('includes/head'); ?>

<body class="error404 text-center">
    <div class="container-fluid error-content">
        <div class="">
            <h1 class="error-number">404</h1>
            <p class="mini-text">Ooops!</p>
            <p class="error-text mb-4 mt-1"><?php echo $this->lang->line('istediginiz_sayfa_yok'); ?></p>
            <a href="<?php echo site_url(); ?>" class="btn btn-primary mt-5"><?php echo $this->lang->line('ana_sayfaya_don'); ?></a>
        </div>
    </div>    
    <script src="<?php echo site_url('resources') ?>/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?php echo site_url('resources') ?>/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo site_url('resources') ?>/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>