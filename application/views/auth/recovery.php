<?php $this->load->view('includes/head'); ?>

<body class="form">
    <div class="form-container outer">
        <div class="form-form">
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">

                        <h1 class=""><?php echo $this->lang->line('sifre_sifirla'); ?></h1>
                        <p class="signup-link recovery"><?php echo $this->lang->line('sifre_sifirlama_metin'); ?></p>

                        <?php if ($this->session->flashdata('recovery_message')) { ?>

                            <div class="alert alert-warning mb-4" role="alert"><?php echo $this->session->flashdata('recovery_message'); ?></div>

                        <?php } ?>

                        <form class="text-left" method="post" action="<?php echo site_url('auth/recovery'); ?>">
                            <div class="form">
                                <div id="email-field" class="field-wrapper input">
                                    <div class="d-flex justify-content-between">
                                        <label for="email"><?php echo $this->lang->line('eposta'); ?></label>
                                    </div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-at-sign">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94"></path>
                                    </svg>
                                    <input id="email" name="email" type="text" class="form-control" value="" placeholder="<?php echo $this->lang->line('eposta'); ?>" required autofocus>
                                </div>
                                <div class="d-sm-flex justify-content-between">
                                    <div class="field-wrapper">
                                        <button type="submit" name="submit" class="btn btn-primary"><?php echo $this->lang->line('gonder'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('includes/scripts'); ?>

</body>

</html>