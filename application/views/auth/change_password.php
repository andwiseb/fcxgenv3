<?php $this->load->view('includes/head'); ?>

<body class="form">
    <div class="form-container outer">
        <div class="form-form">
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">
                        <h1 class="mb-4"><?php echo $this->lang->line('sifre_degistir'); ?></h1>

                        <?php if ($this->session->flashdata('change_password_message')) { ?>

                        <div class="alert alert-warning mb-4" role="alert"><?php echo $this->session->flashdata('change_password_message'); ?></div> 

                        <?php } ?>

                        <form class="text-left" method="post" action="<?php echo site_url('auth/change_password'); ?>">
                            <div class="form">
                                <div id="email-field" class="input mb-3">
                                    <input name="password" type="password" class="form-control" placeholder="<?php echo $this->lang->line('sifre'); ?>" required autofocus>
                                </div>
                                <div id="email-field" class="input mb-3">
                                    <input name="confirm-password" type="password" class="form-control" placeholder="<?php echo $this->lang->line('sifreyi_onayla'); ?>" required>
                                </div>
                                <input type="hidden" value="<?php echo $code; ?>" name="code">
                                <div class="d-sm-flex justify-content-between">
                                    <div class="field-wrapper">
                                        <button type="submit" name="submit" class="btn btn-primary"><?php echo $this->lang->line('gonder'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    
    <?php $this->load->view('includes/scripts'); ?>

</body>
</html>