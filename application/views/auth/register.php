<?php $this->load->view('includes/head'); ?>

<body class="form">
    <div class="form-container outer">
        <div class="form-form">
            <div class="form-form-wrap">
                <div class="form-container">
                    <div class="form-content">
                        <h1 class=""><?php echo $title; ?></h1>
                        <p class="signup-link register"><?php echo $this->lang->line('uye_misin'); ?>
                            <a href="<?php echo site_url('login') ?>"><?php echo $this->lang->line('giris_yap'); ?></a>
                        </p>

                        <?php if ($this->session->flashdata('register_message')) { ?>

                            <div class="alert alert-warning mb-4" role="alert"><?php echo $this->session->flashdata('register_message'); ?></div>

                        <?php } ?>

                        <form class="text-left" method="post" action="<?php echo site_url('auth/register'); ?>">
                            <div class="form">
                                <div id="email-field" class="field-wrapper input">
                                    <label for="email"><?php echo $this->lang->line('eposta'); ?></label>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-at-sign register">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94"></path>
                                    </svg>
                                    <input name="email" type="email" value="" class="form-control" placeholder="<?php echo $this->lang->line('eposta'); ?>" required>
                                </div>
                                <div id="username-field" class="field-wrapper input">
                                    <label for="username"><?php echo $this->lang->line('kullanici_adi'); ?></label>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user">
                                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="12" cy="7" r="4"></circle>
                                    </svg>
                                    <input name="username" type="text" value="" class="form-control" placeholder="<?php echo $this->lang->line('kullanici_adi'); ?>" required>
                                </div>
                                <div id="password-field" class="field-wrapper input mb-2">
                                    <div class="d-flex justify-content-between">
                                        <label for="password"><?php echo $this->lang->line('sifre'); ?></label>
                                    </div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock">
                                        <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
                                        <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                                    </svg>
                                    <input name="password" id="password" type="password" class="form-control" placeholder="<?php echo $this->lang->line('sifre'); ?>" required>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" data-pass="hideShow" id="passwordSvg" data-input-name="password"  viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" id="toggle-password" class="feather feather-eye">
                                        <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                        <circle cx="12" cy="12" r="3"></circle>
                                    </svg>
                                </div>
                                <div id="password-field" class="field-wrapper input mb-2">
                                    <div class="d-flex justify-content-between">
                                        <label for="password"><?php echo $this->lang->line('sifreyi_onayla'); ?></label>
                                    </div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock">
                                        <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
                                        <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                                    </svg>
                                    <input name="confirm-password" id="confirm-password" type="password" class="form-control" placeholder="<?php echo $this->lang->line('sifreyi_onayla'); ?>" required>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" data-pass="hideShow" id="confirmPassword" data-input-name="confirm-password" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" id="toggle-password" class="feather feather-eye" >
                                        <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                        <circle cx="12" cy="12" r="3"></circle>
                                    </svg>
                                </div>
                                <div class="field-wrapper terms_condition">
                                    <div class="n-chk new-checkbox checkbox-outline-primary">
                                        <label class="new-control new-checkbox checkbox-outline-primary">
                                            <input type="checkbox" name="terms_register" class="new-control-input">
                                            <span class="new-control-indicator"></span><span><?php echo $this->lang->line('politika_gizlilik_kontrol'); ?></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="d-sm-flex justify-content-between">
                                    <div class="field-wrapper">
                                        <button type="submit" name="submit" class="btn btn-primary"><?php echo $this->lang->line('gonder'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('includes/scripts'); ?>

</body>

</html>