<?php $settings = get_settings(); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <meta name='description' content="<?php echo $settings->description; ?>">
        <meta name='og:title' content="<?php echo $title; ?> - <?php echo $settings->title; ?>">
        <meta name='og:image' content="https://sellthing.co/resources/assets/img/coming-soon/coming-soon.png">
        <meta name='og:site_name' content="<?php echo $settings->title; ?>">
        <meta name='og:description' content="<?php echo $settings->description; ?>">
        <title><?php echo $title; ?> - <?php echo $settings->title; ?></title>
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
        <link href="<?php echo base_url('resources') ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/plugins.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/pages/coming-soon/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/forms/theme-checkbox-radio.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/forms/switches.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/font-icons/font-awesome/css/all.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/elements/avatar.css" rel="stylesheet" type="text/css" />
        <link rel="icon" type="image/x-icon" href="<?php echo base_url('resources/assets/img/') . $settings->favicon; ?>"/>
        <style>
            .avatar-sm {
                width: 1.5rem;
                height: 1.5rem;
                font-size: .83333rem;
            }
            .specialLanguage{
                position: absolute;
                top: 0px;
                margin-left: 83px!important;
            }
        </style>
    </head>
<body class="coming-soon">
    <div class="coming-soon-container">
        <div class="coming-soon-cont">
            <div class="coming-soon-wrap">
                <div class="coming-soon-container">
                    <div class="coming-soon-content">
                        <ul class="specialLanguage social list-inline">
                            <li class="list-inline-item">
                                <a class="" href="<?php echo site_url(); ?>">
                                    <div class="avatar avatar-sm">
                                        <img alt="avatar" src="<?php echo base_url('resources') ?>/assets/img/language/en.png" class="rounded-circle" />
                                    </div>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="" href="<?php echo base_url('fr'); ?>">
                                    <div class="avatar avatar-sm">
                                        <img alt="avatar" src="<?php echo base_url('resources') ?>/assets/img/language/fr.png" class="rounded-circle" />
                                    </div>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="" href="<?php echo base_url('pr'); ?>">
                                    <div class="avatar avatar-sm">
                                        <img alt="avatar" src="<?php echo base_url('resources') ?>/assets/img/language/pr.png" class="rounded-circle" />
                                    </div>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="" href="<?php echo base_url('tr'); ?>">
                                    <div class="avatar avatar-sm">
                                        <img alt="avatar" src="<?php echo base_url('resources') ?>/assets/img/language/tr.png" class="rounded-circle" />
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <h4 class=""><?php echo $this->lang->line('cok_yakinda'); ?></h4>
                        <p class=""><?php echo $this->lang->line('cok_yakinda_aciklama'); ?></p>

                        <div id="timer">
                            <div class="days"><span class="count">--</span> <span class="text"><?php echo $this->lang->line('gun'); ?></span></div>
                            <div class="hours"><span class="count">--</span> <span class="text"><?php echo $this->lang->line('saat'); ?></span></div>
                            <div class="min"><span class="count">--</span> <span class="text"><?php echo $this->lang->line('dakika'); ?></span></div>
                            <div class="sec"><span class="count">--</span> <span class="text"><?php echo $this->lang->line('saniye'); ?></span></div>
                        </div>

                        <h3><?php echo $this->lang->line('bildirim_alin'); ?></h3>
                        <h3 class="<?php echo ($this->session->flashdata('subscribe')) ? '' : 'd-none'; ?> "><?php echo $this->session->flashdata('subscribe'); ?></h3>
                        <form class="text-left" method="POST" action="<?php echo site_url('misc/subscribe') ?>">
                            <div class="coming-soon">
                                <div class="">
                                    <div id="email-field" class="field-wrapper input">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-at-sign">
                                            <circle cx="12" cy="12" r="4"></circle>
                                            <path d="M16 8v5a3 3 0 0 0 6 0v-1a10 10 0 1 0-3.92 7.94"></path>
                                        </svg>
                                        <input id="email" name="email" class="form-control" type="text" value="" placeholder="<?php echo $this->lang->line('eposta'); ?>" required>
                                        <button type="submit" class="btn btn-success" value=""><?php echo $this->lang->line('abone_ol'); ?></button>
                                    </div>
                                </div>

                            </div>
                        </form>

                        <ul class="social list-inline">
                            <li class="list-inline-item">
                                <a class="" href="https://discord.gg/eYE8EEgF6C">
                                    <i class="fab fa-discord fa-2x" style="color:#009688;"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="" href="https://twitter.com/fcxgen">
                                    <i class="fab fa-twitter-square fa-2x" style="color:#009688;"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="" href="https://www.youtube.com/channel/UCa87_Bnc0zVaA65UjcacQug">
                                    <i class="fab fa-youtube-square fa-2x" style="color:#009688;"></i>
                                </a>
                            </li>
                        </ul>

                        <p class="terms-conditions">
                            © <?php echo date('Y'); ?> <?php echo $this->lang->line('tum_haklari_saklidir'); ?>
                            <!-- <a href="javascript:void(0);"><?php echo $this->lang->line('cerez_kullanimi'); ?></a>, 
                            <a href="javascript:void(0);"><?php echo $this->lang->line('gizlilik_sozlesmesi'); ?></a> <?php echo $this->lang->line('ve'); ?>
                            <a href="javascript:void(0);"><?php echo $this->lang->line('kullanim_sartlari'); ?></a>. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="coming-soon-image">
            <div class="l-image">
                <div class="img-content" style="background: url(<?php echo base_url('resources') ?>/assets/img/coming-soon/coming-soon.png); background-position: left top;">
                    <div class="w-100 h-100 display-absolute" style="background: rgb(31,48,110); background: linear-gradient(133deg, rgb(31 48 110 / 39%) 0%, rgba(31,48,110,1) 100%); opacity: 0.8;"></div>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url('resources') ?>/assets/js/libs/jquery-3.1.1.min.js"></script>
        <script src="<?php echo base_url('resources') ?>/bootstrap/js/popper.min.js"></script>
        <script src="<?php echo base_url('resources') ?>/bootstrap/js/bootstrap.min.js"></script>
        <?php $this->load->view('misc/countdown_js'); ?>
    </body>
</html>