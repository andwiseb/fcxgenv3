<?php echo '<?xml version="1.0" encoding="UTF-8" ?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo base_url(); ?></loc> 
        <priority>1.0</priority>
    </url>

    <?php 
    
    for ($i = 0; $i < count($languages); $i++) {
        if ($languages[$i] == 'en') {
            continue;
        }
        
    ?>
      
        <url>
            <loc><?php echo base_url($languages[$i]); ?></loc>
            <priority>0.5</priority>
        </url>

    <?php } ?>

    <?php 
    
    foreach ($pages->result() as $page) {
        for ($i = 0; $i < count($languages); $i++) {
            
    ?>

    <url>
        <loc><?php echo base_url($languages[$i] . '/' . $page->url); ?></loc>
        <priority>0.5</priority>
    </url>

    <?php } } ?>

</urlset>