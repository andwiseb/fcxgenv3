<?php

$this->load->view('includes/head');
$this->load->view('includes/menu');

?>

<h3><?php echo $title; ?></h3>

<?php if ($this->session->flashdata('accounts_message')) { ?>

    <div class="alert alert-warning mb-4 mt-4" role="alert"><?php echo $this->session->flashdata('accounts_message'); ?></div>

<?php } ?>

<div class="alert alert-warning mb-4 mt-4 d-none" role="alert"></div>
<ul class="nav nav-tabs mb-3 mt-3 nav-fill" id="accountsTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="list-tab" data-toggle="tab" href="#list" role="tab" aria-controls="list" aria-selected="true"><?php echo $this->lang->line('liste'); ?></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="single-tab" data-toggle="tab" href="#single" role="tab" aria-controls="single" aria-selected="false"><?php echo $this->lang->line('tekli_ekle'); ?></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="bulk-tab" data-toggle="tab" href="#bulk" role="tab" aria-controls="bulk" aria-selected="false"><?php echo $this->lang->line('coklu_ekle'); ?></a>
    </li>
</ul>
<div class="tab-content" id="accountsTabContent">
    <div class="tab-pane fade show active" id="list" role="tabpanel" aria-labelledby="list-tab">
        <div class="table-responsive">
            <table id="zero-config" class="table table-bordered table-striped mb-4" style="width:100%">
                <thead>
                    <tr>
                        <th><?php echo $this->lang->line('kullanici'); ?></th>
                        <th><?php echo $this->lang->line('eposta'); ?></th>
                        <th><?php echo $this->lang->line('sifre'); ?></th>
                        <th><?php echo $this->lang->line('platform'); ?></th>
                        <th><?php echo $this->lang->line('tur'); ?></th>
                        <th><?php echo $this->lang->line('kod'); ?></th>
                        <th><?php echo $this->lang->line('yuklenme_zamani'); ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php

                    $arr = ["plextv", "plex", "plex tv"];
                    $arr = "PlexTV";

                    if ($accounts->num_rows() > 0) {
                        foreach ($accounts->result() as $account) {

                    ?>

                            <tr>
                                <td>
                                    <div class="d-flex">
                                        <div class="usr-img-frame p-0 rounded-circle bg-none">
                                            <img alt="avatar" class="img-fluid rounded-circle w-100 h-100" src="<?php echo base_url('uploads/user-img/') . $account->img; ?>">
                                        </div>
                                    </div>
                                </td>
                                <td><?php echo $account->a_email; ?></td>
                                <td><?php echo $account->a_password; ?></td>
                                <td><?php echo $account->p_title; ?></td>
                                <td><?php echo $account->at_title; ?></td>
                                <td><?php echo $account->code; ?></td>
                                <td><?php echo $account->upload_time; ?></td>
                            </tr>

                        <?php } } else { ?>

                        <tr>
                            <td colspan="6" class="text-center"><?php echo $this->lang->line('hesap_bulunamadi'); ?></td>
                        </tr>

                    <?php } ?>

                </tbody>
            </table>
        </div>
        <hr>
        <div class="form-row mt-4 text-center">
            <div class="form-group col-md-12">
                <a href="<?php echo site_url('admin'); ?>" class="btn btn-danger btn-lg mb-1">
                    <i class="far fa-arrow-alt-circle-left"></i>&nbsp;&nbsp;<?php echo $this->lang->line('panele_geri_don'); ?>
                </a>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="single" role="tabpanel" aria-labelledby="single">
        <form action="<?php echo site_url('admin/single_add_account'); ?>" method="post" class="form">
            <div class="form-row mb-2 mt-4">
                <div class="form-group col-md-3">
                    <label for="mailPass"><?php echo $this->lang->line('eposta_sifre'); ?> *</label>
                    <input type="text" class="form-control" id="mailPass" name="mail-pass" required>
                </div>
                <div class="form-group col-md-3">
                    <label for="caption"><?php echo $this->lang->line('ayrintilar'); ?></label>
                    <input type="text" class="form-control" id="caption" name="caption">
                </div>
                <div class="form-group col-md-3">
                    <label for="accountPlatform"><?php echo $this->lang->line('platform'); ?> *</label>
                    <select class="form-control" id="accountPlatform" name="account-platform" required>
                        <option value="0"><?php echo $this->lang->line('sec'); ?>...</option>

                        <?php foreach ($platforms->result() as $platform) { ?>

                            <option value="<?php echo $platform->id; ?>"><?php echo $platform->title; ?></option>

                        <?php } ?>

                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label><?php echo $this->lang->line('tur'); ?> *</label>
                    <div class="form-check mb-2">
                        <div class="custom-control custom-radio classic-radio-info">
                            <input type="radio" id="typeFree" name="account-type" class="custom-control-input" value="1" checked>
                            <label class="custom-control-label" for="typeFree"><?php echo $this->lang->line('ucretsiz'); ?></label>
                        </div>
                    </div>
                    <div class="form-check mb-2">
                        <div class="custom-control custom-radio classic-radio-info">
                            <input type="radio" id="typePaid" name="account-type" class="custom-control-input" value="2">
                            <label class="custom-control-label" for="typePaid"><?php echo $this->lang->line('ucretli'); ?></label>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-row mt-4 text-center">
                <div class="form-group col-md-12">
                    <button type="submit" name="submit" class="btn btn-primary btn-lg mr-2 mb-1">
                        <i class="far fa-save"></i>&nbsp;&nbsp;<?php echo $this->lang->line('kaydet'); ?>
                    </button>
                    <a href="<?php echo site_url('admin'); ?>" class="btn btn-danger btn-lg mb-1 return">
                        <i class="far fa-arrow-alt-circle-left"></i>&nbsp;&nbsp;<?php echo $this->lang->line('panele_geri_don'); ?>
                    </a>
                    <a href="<?php echo site_url('accounts'); ?>" class="btn btn-danger btn-lg mb-1 cancel d-none">
                        <i class="far fa-window-close"></i>&nbsp;&nbsp;<?php echo $this->lang->line('duzenleme_iptali'); ?>
                    </a>
                </div>
            </div>
        </form>
    </div>
    <div class="tab-pane fade" id="bulk" role="tabpanel" aria-labelledby="bulk">
        <form action="<?php echo site_url('admin/bulk_add_account'); ?>" method="post" class="form" enctype="multipart/form-data">
            <div class="form-row mb-2 mt-4">
                <div class="form-group col-md-4">
                    <label for="baccountsFile"><?php echo $this->lang->line('dosya'); ?> *</label>
                    <input type="file" class="form-control-file" id="baccountsFile" name="b-accounts-file">
                </div>
                <div class="form-group col-md-4">
                    <label for="baccountPlatform"><?php echo $this->lang->line('platform'); ?> *</label>
                    <select class="form-control" id="baccountPlatform" name="b-account-platform" required>
                        <option value="0"><?php echo $this->lang->line('sec'); ?>...</option>

                        <?php foreach ($platforms->result() as $platform) { ?>

                            <option value="<?php echo $platform->id; ?>"><?php echo $platform->title; ?></option>

                        <?php } ?>

                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label><?php echo $this->lang->line('tur'); ?> *</label>
                    <div class="form-check mb-2">
                        <div class="custom-control custom-radio classic-radio-info">
                            <input type="radio" id="btypeFree" name="b-account-type" class="custom-control-input" value="1" checked>
                            <label class="custom-control-label" for="btypeFree"><?php echo $this->lang->line('ucretsiz'); ?></label>
                        </div>
                    </div>
                    <div class="form-check mb-2">
                        <div class="custom-control custom-radio classic-radio-info">
                            <input type="radio" id="btypePaid" name="b-account-type" class="custom-control-input" value="2">
                            <label class="custom-control-label" for="btypePaid"><?php echo $this->lang->line('ucretli'); ?></label>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-row mt-4 text-center">
                <div class="form-group col-md-12">
                    <button type="submit" name="submit" class="btn btn-primary btn-lg mr-2 mb-1">
                        <i class="far fa-save"></i>&nbsp;&nbsp;<?php echo $this->lang->line('kaydet'); ?>
                    </button>
                    <a href="<?php echo site_url('admin'); ?>" class="btn btn-danger btn-lg mb-1 return">
                        <i class="far fa-arrow-alt-circle-left"></i>&nbsp;&nbsp;<?php echo $this->lang->line('panele_geri_don'); ?>
                    </a>
                    <a href="<?php echo site_url('accounts'); ?>" class="btn btn-danger btn-lg mb-1 cancel d-none">
                        <i class="far fa-window-close"></i>&nbsp;&nbsp;<?php echo $this->lang->line('duzenleme_iptali'); ?>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>