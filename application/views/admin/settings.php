<?php 

$this->load->view('includes/head'); 
$this->load->view('includes/menu'); 

?>

<h3><?php echo $title; ?></h3>

<?php if ($this->session->flashdata('settings_message')) { ?>

<div class="alert alert-warning mb-4 mt-4" role="alert"><?php echo $this->session->flashdata('settings_message'); ?></div> 

<?php } ?>

<ul class="nav nav-tabs mb-3 mt-3 nav-fill" id="settingsTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true"><?php echo $this->lang->line('genel_ayarlar'); ?></a>
    </li>
</ul>
<div class="tab-content" id="settingsTabContent">
    <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
        <form action="<?php echo site_url('admin/update_settings'); ?>" method="post" enctype="multipart/form-data">
            <div class="form-row mb-2 mt-4">
                <div class="form-group col-md-6">
                    <label for="siteTitle"><?php echo $this->lang->line('site_basligi'); ?> *</label>
                    <input type="text" class="form-control" id="siteTitle" name="site-title" value="<?php echo $settings->title; ?>" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="siteDescription"><?php echo $this->lang->line('site_aciklamasi'); ?> *</label>
                    <input type="text" class="form-control" id="siteDescription" name="site-description" value="<?php echo $settings->description; ?>" required>
                </div>
            </div>
            <div class="form-row mb-2 mt-2">
                <div class="form-group col-md-4 d-flex">
                    <div>
                        <label for="siteLogo"><?php echo $this->lang->line('logo'); ?> *</label>
                        <input type="file" class="form-control-file" id="siteLogo" name="site-logo">
                    </div>
                    <div class="avatar avatar-lg text-right">
                        <img alt="avatar" src="<?php echo base_url('resources/assets/img/') . $settings->logo; ?>" class="rounded-circle" />
                    </div>
                </div>
                <div class="form-group col-md-4 d-flex">
                    <div>
                        <label for="siteFavicon"><?php echo $this->lang->line('favicon'); ?> *</label>
                        <input type="file" class="form-control-file" id="siteFavicon" name="site-favicon">
                    </div>
                    <div class="avatar avatar-lg text-right">
                        <img alt="avatar" src="<?php echo base_url('resources/assets/img/') . $settings->favicon; ?>" class="rounded-circle" />
                    </div>
                </div>
                <div class="form-group col-md-4 d-flex">
                    <div>
                        <label for="metaImage"><?php echo $this->lang->line('meta_gorsel'); ?> *</label>
                        <input type="file" class="form-control-file" id="metaImage" name="meta-image">
                    </div>
                    <div class="avatar avatar-lg text-right">
                        <img alt="avatar" src="<?php echo base_url('resources/assets/img/') . $settings->meta_image; ?>" class="rounded-circle" />
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-row mt-4 text-center">
                <div class="form-group col-md-12">
                    <button type="submit" name="submit" class="btn btn-primary btn-lg mr-2 mb-1">
                        <i class="far fa-save"></i>&nbsp;&nbsp;<?php echo $this->lang->line('kaydet'); ?>
                    </button>
                    <a href="<?php echo site_url('admin'); ?>" class="btn btn-danger btn-lg mb-1">
                        <i class="far fa-arrow-alt-circle-left"></i>&nbsp;&nbsp;<?php echo $this->lang->line('panele_geri_don'); ?>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>