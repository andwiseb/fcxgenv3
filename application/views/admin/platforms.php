<?php 

$this->load->view('includes/head'); 
$this->load->view('includes/menu'); 

?>

<h3><?php echo $title; ?></h3>

<?php if ($this->session->flashdata('platforms_message')) { ?>

<div class="alert alert-warning mb-4 mt-4" role="alert"><?php echo $this->session->flashdata('platforms_message'); ?></div> 

<?php } ?>

<div class="alert alert-warning mb-4 mt-4 d-none" role="alert"></div>
<ul class="nav nav-tabs mb-3 mt-3 nav-fill" id="platformsTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="list-tab" data-toggle="tab" href="#list" role="tab" aria-controls="list" aria-selected="true"><?php echo $this->lang->line('liste'); ?></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="add-tab" data-toggle="tab" href="#add" role="tab" aria-controls="add" aria-selected="false"><?php echo $this->lang->line('ekle_duzenle'); ?></a>
    </li>
</ul>
<div class="tab-content" id="platformsTabContent">
    <div class="tab-pane fade show active" id="list" role="tabpanel" aria-labelledby="list-tab">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped table-checkable table-highlight-head mb-1">
                <thead>
                    <tr>
                        <th class=""><?php echo $this->lang->line('baslik'); ?></th>
                        <th class=""><?php echo $this->lang->line('url'); ?></th>
                        <th class="text-center" width="170px"><?php echo $this->lang->line('eylem'); ?></th>
                    </tr>
                </thead>
                <tbody>

                <?php 
                
                if ($platforms->num_rows() > 0) {
                    foreach ($platforms->result() as $platform) { 
                    
                ?>

                    <tr>
                        <td>
                            <p class="mb-0"><?php echo $platform->title; ?></p>
                        </td>
                        <td>
                            <a href="<?php echo site_url('platform/') . $platform->url; ?>"><?php echo site_url('platform/') . $platform->url; ?></a>
                        </td>
                        <td class="text-center">
                            <ul class="table-controls">
                                <li>
                                    <a href="javascript:void(0);" class="edit" data-platform-id="<?php echo $platform->id; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $this->lang->line('duzenle'); ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 text-success">
                                            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" class="delete" data-platform-id="<?php echo $platform->id; ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $this->lang->line('sil'); ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 text-danger">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </td>
                    </tr>

                <?php } } else { ?>

                    <tr>
                        <td colspan="4" class="text-center"><?php echo $this->lang->line('platform_bulunamadi'); ?></td>
                    </tr>

                <?php } ?>

                </tbody>
            </table>
        </div>
        <hr>
        <div class="form-row mt-4 text-center">
            <div class="form-group col-md-12">
                <a href="<?php echo site_url('admin'); ?>" class="btn btn-danger btn-lg mb-1">
                    <i class="far fa-arrow-alt-circle-left"></i>&nbsp;&nbsp;<?php echo $this->lang->line('panele_geri_don'); ?>
                </a>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="add" role="tabpanel" aria-labelledby="add">
        <form action="<?php echo site_url('admin/add_platform'); ?>" method="post" class="form" enctype="multipart/form-data">
            <input type="hidden" class="form-control" id="platformId" name="platform-id">
            <div class="form-row mb-2 mt-4">
                <div class="form-group col-md-6">
                    <label for="platformName"><?php echo $this->lang->line('baslik'); ?> *</label>
                    <input type="text" class="form-control" id="platformName" name="platform-name" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="platformUrl"><?php echo $this->lang->line('url'); ?> *</label>
                    <input type="text" class="form-control" id="platformUrl" name="platform-url" readonly>
                </div>
            </div>
            <div class="form-row mb-2 mt-2">
                <div class="form-group col-md-6 d-flex">
                    <div>
                        <label for="platformImage"><?php echo $this->lang->line('gorsel'); ?> *</label>
                        <input type="file" class="form-control-file" id="platformImage" name="platform-image" required>
                    </div>
                    <div class="avatar avatar-lg text-right d-none">
                        <img alt="avatar" class="rounded-circle" />
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="platformCategory"><?php echo $this->lang->line('kategori'); ?> *</label>
                    <select class="form-control" id="platformCategory" name="platform-category" required>
                        <option value="0"><?php echo $this->lang->line('sec'); ?>...</option>

                        <?php foreach ($categories->result() as $category) { ?>

                        <option value="<?php echo $category->id; ?>"><?php echo $category->title; ?></option>

                        <?php } ?>

                    </select>
                </div>
            </div>
            <div class="form-row mb-2 mt-2">
                <div class="form-group col-md-4">
                    <label for="metaTitle"><?php echo $this->lang->line('meta_baslik'); ?> *</label>
                    <input type="text" class="form-control" id="metaTitle" name="meta-title" required>
                </div>
                <div class="form-group col-md-4">
                    <label for="metaDescription"><?php echo $this->lang->line('meta_aciklama'); ?> *</label>
                    <input type="text" class="form-control" id="metaDescription" name="meta-description" required>
                </div>
                <div class="form-group col-md-4">
                    <label for="metaKeywords"><?php echo $this->lang->line('meta_anahtar_kelimeler'); ?> *</label>
                    <input type="text" class="form-control" id="metaKeywords" name="meta-keywords" required>
                </div>
            </div>
            <hr>
            <div class="form-row mt-4 text-center">
                <div class="form-group col-md-12">
                    <button type="submit" name="submit" class="btn btn-primary btn-lg mr-2 mb-1">
                        <i class="far fa-save"></i>&nbsp;&nbsp;<?php echo $this->lang->line('kaydet'); ?>
                    </button>
                    <a href="<?php echo site_url('admin'); ?>" class="btn btn-danger btn-lg mb-1 return">
                        <i class="far fa-arrow-alt-circle-left"></i>&nbsp;&nbsp;<?php echo $this->lang->line('panele_geri_don'); ?>
                    </a>
                    <a href="<?php echo site_url('admin/platforms'); ?>" class="btn btn-danger btn-lg mb-1 cancel d-none">
                        <i class="far fa-window-close"></i>&nbsp;&nbsp;<?php echo $this->lang->line('duzenleme_iptali'); ?>
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>