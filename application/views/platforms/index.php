<?php

$this->load->view('includes/head');
$this->load->view('includes/menu');

?>

<style>

    .platform-header-image {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        pointer-events: none;
        height: 280px;
        background-color: #3b3f5c;
        background-image: url(<?php echo base_url('resources/assets/img/') . $category->image; ?>);
        background-attachment: fixed;
        background-size: cover;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }

    .helpdesk:before {
        content: none;
    }
    
    .carousel-control-prev{
        background-color: transparent;
    }

    .carousel-control-next{
        background-color: transparent;
    }

</style>

<div class="helpdesk container">
    <div class="platform-header-image"></div>
    <div class="helpdesk layout-spacing">
        <div class="hd-header-wrapper">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h4 class=""><?php echo $title; ?></h4>
                    <a href="<?php echo site_url('generate/') . $platform->url; ?>" class="btn btn-primary mt-3"><?php echo $this->lang->line('olustur'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>