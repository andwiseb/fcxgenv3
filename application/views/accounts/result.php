<?php 

$this->load->view('includes/head'); 
$this->load->view('includes/menu'); 

?>

<div class="row row mb-4 p-5">
    <div class="col-sm-6 col-12">
        <label for="inputEmail4"><?php echo $this->lang->line('eposta'); ?></label>
        <div class="input-group mb-4">
            <div class="input-group-prepend">
                <a class="input-group-text" href="javascript:;" data-clipboard-action="copy" data-clipboard-target="#email">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-copy">
                        <rect x="9" y="9" width="13" height="13" rx="2" ry="2"></rect>
                        <path d="M5 15H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v1"></path>
                    </svg>
                </a>
            </div>
            <input type="text" class="form-control" id="email" value="<?php echo $email; ?>" readonly>
        </div>
    </div>
    <div class="col-sm-6 col-12">
        <label for="inputEmail4"><?php echo $this->lang->line('sifre'); ?></label>
        <div class="input-group mb-4">
            <div class="input-group-prepend">
                <a class="input-group-text" href="javascript:;" data-clipboard-action="copy" data-clipboard-target="#password">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-copy">
                        <rect x="9" y="9" width="13" height="13" rx="2" ry="2"></rect>
                        <path d="M5 15H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v1"></path>
                    </svg>
                </a>
            </div>
            <input type="text" class="form-control" id="password" value="<?php echo $password; ?>" readonly>
        </div>
    </div>
    <div class="col-sm-12 col-12 mt-4">
        <label for="inputEmail4"><?php echo $this->lang->line('ayrintilar'); ?></label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" readonly><?php echo $caption; ?></textarea>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>