<?php 

$this->load->view('includes/head'); 
$this->load->view('includes/menu'); 

?>

<div class="row mb-4 mt-3">
    <div class="col-sm-2 col-12">
        <div class="nav flex-column nav-pills mb-sm-0 mb-3" id="categories-tab" role="tablist" aria-orientation="vertical">

            <?php 
            
            $count = 0;

            foreach ($categories as $category) { 
                
            ?>

            <a class="nav-link <?php echo ($count == 0) ? 'active' : ''; ?> mb-2" id="<?php echo $category->url; ?>-tab" data-toggle="pill" href="#<?php echo $category->url; ?>" role="tab" aria-controls="v-pills-home" aria-selected="true"><?php echo $category->title; ?></a>
        
            <?php $count++; } ?>
        
        </div>
    </div>
    <div class="col-sm-10 col-12">
        <div class="tab-content" id="categories-tab-content">

            <?php 
            
            $count = 0;

            foreach ($categories as $category) { 
                
            ?>

            <div class="tab-pane fade <?php echo ($count == 0) ? 'show active' : ''; ?>" id="<?php echo $category->url; ?>" role="tabpanel" aria-labelledby="<?php echo $category->url; ?>-tab">
                <div class="row">

                    <?php 
                    
                    if (get_platforms_with_category($category->id)->num_rows() > 0) {
                        foreach (get_platforms_with_category($category->id)->result() as $platform) { 
                        
                    ?>

                    <div class="col-md-3">
                        <div class="card component-card_2">
                            <img src="<?php echo base_url('resources/assets/img/') . $platform->image; ?>" class="card-img-top" width="286px" height="150px">
                            <div class="card-body text-center">
                                <a href="<?php echo site_url('generate/') . $platform->url; ?>" class="btn btn-primary"><?php echo $this->lang->line('olustur'); ?></a>
                            </div>
                        </div>
                    </div>

                    <?php } } else { ?>

                    <p><?php print_r($categories->result()); ?></p>

                    <?php } ?>

                </div>
            </div>

            <?php $count++; } ?>

        </div>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>