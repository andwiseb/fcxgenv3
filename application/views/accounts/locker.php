<?php

$this->load->view('includes/head');
$this->load->view('includes/menu');

?>
<div class="row">
    <div class="col-12">
        <div class="lockerDiv">
            <div class="fa-3x">
                <i class="fas fa-spinner fa-pulse"></i>
            </div>
            <p>

            <?php echo $this->lang->line('kilit_aciklama'); ?>
            
            </p>
            <button data-click="locker" class="btn btn-primary btn-locker-social btn-facebook">
                <i class="fa-lg fab fa-facebook"></i> <span><?php echo $this->lang->line('paylas'); ?></span>
            </button>
            <button data-click="locker" class="btn btn-primary btn-locker-social btn-twitter">
                <i class="fa-lg fab fa-twitter"></i> <span><?php echo $this->lang->line('paylas'); ?></span>
            </button>
            <button data-click="locker" class="btn btn-primary btn-locker-social btn-youtube">
                <i class="fa-lg fab fa-youtube"></i> <span><?php echo $this->lang->line('takip_et'); ?></span>
            </button>

            <?php if (id_user_info($this->session->userdata('id'))->ig_username == '') { ?>

            <button data-click="locker" class="btn btn-primary btn-locker-social btn-instagram">
                <i class="fa-lg fab fa-instagram"></i> <span><?php echo $this->lang->line('takip_et'); ?></span>
            </button>

            <?php } ?>

        </div>
    </div>
</div>
<div class="row row mb-4 p-5 resultInput lockInput">
    <div class="col-sm-6 col-12">
        <label for="inputEmail4"><?php echo $this->lang->line('eposta'); ?></label>
        <div class="input-group mb-4">
            <div class="input-group-prepend">
                <a class="input-group-text" href="javascript:;" id="copyMail" onclick="mailCopy()">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-copy">
                        <rect x="9" y="9" width="13" height="13" rx="2" ry="2"></rect>
                        <path d="M5 15H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v1"></path>
                    </svg>
                </a>
            </div>
            <input type="text" class="form-control" id="email" readonly>
        </div>
    </div>
    <div class="col-sm-6 col-12">
        <label for="inputEmail4"><?php echo $this->lang->line('sifre'); ?></label>
        <div class="input-group mb-4">
            <div class="input-group-prepend">
                <a class="input-group-text" href="javascript:;" id="copyPass" onclick="passCopy()">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-copy">
                        <rect x="9" y="9" width="13" height="13" rx="2" ry="2"></rect>
                        <path d="M5 15H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v1"></path>
                    </svg>
                </a>
            </div>
            <input type="text" class="form-control" id="password" readonly>
        </div>
    </div>
    <div class="col-sm-12 col-12 mt-4">
        <label for="inputEmail4"><?php echo $this->lang->line('ayrintilar'); ?></label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" readonly></textarea>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>