<?php 

$settings = get_settings();
$url = $this->uri->uri_string();
$exploded = explode($this->session->userdata('language') . '/', $url);
$url = implode('', $exploded);

?>

<title><?php echo ($this->uri->segment(2) == 'home') ? $settings->title . ' - ' . $settings->description : $title . ' - ' . $settings->title; ?></title>
<meta name="title" content="<?php echo get_meta_tag($url)->title . ' - ' . $settings->title; ?>">
<meta name="description" content="<?php echo get_meta_tag($url)->description; ?>">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo base_url() . $this->uri->uri_string(); ?>">
<meta property="og:title" content="<?php echo get_meta_tag($url)->title . ' - ' . $settings->title; ?>">
<meta property="og:description" content="<?php echo get_meta_tag($url)->description; ?>">
<meta property="og:image" content="<?php echo (get_meta_tag($url)->image == '') ? base_url('resources/assets/img/') . $settings->meta_image : base_url('resources/assets/img/') . get_meta_tag($url)->image; ?>">
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="<?php echo base_url() . $this->uri->uri_string(); ?>">
<meta property="twitter:title" content="<?php echo get_meta_tag($url)->title . ' - ' . $settings->title; ?>">
<meta property="twitter:description" content="<?php echo get_meta_tag($url)->description; ?>">
<meta property="twitter:image" content="<?php echo (get_meta_tag($url)->image == '') ? base_url('resources/assets/img/') . $settings->meta_image : base_url('resources/assets/img/') . get_meta_tag($url)->image; ?>">