<?php if ($this->uri->segment(2) != 'register' || $this->uri->segment(2) != 'login' || $this->uri->segment(2) == 'recovery' || $this->uri->segment(2) == 'change-password') { ?>

    <script src="<?php echo base_url('resources') ?>/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/font-icons/font-awesome/js/all.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/assets/js/app.js"></script>
    <script src="<?php echo base_url('resources') ?>/assets/js/main.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/notification/snackbar/snackbar.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/assets/js/components/notification/custom-snackbar.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
        $('[data-pass="hideShow"]').click(function() {
            var dataInputName = $(this).attr('data-input-name');
            if ($('#' + dataInputName).attr('type') == "text") {
                $('#' + dataInputName).attr('type', 'password')
            } else {
                $('#' + dataInputName).attr('type', 'text')
            }
        });
    </script>
    <script src="<?php echo base_url('resources') ?>/assets/js/custom.js"></script>

<?php } if ($this->uri->segment(2) == 'register' || $this->uri->segment(2) == 'login' || $this->uri->segment(2) == 'recovery' || $this->uri->segment(2) == 'change-password') { ?>

    <script src="<?php echo base_url('resources') ?>/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/bootstrap/js/bootstrap.min.js"></script>

<?php } else if ($this->uri->segment(3) == 'categories') { ?>

    <script src="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/sweetalerts/custom-sweetalert.js"></script>
    <script>
        $('#categoryName').bind('keyup keypress blur', function() {
            var myStr = $(this).val();

            myStr = myStr.toLowerCase();
            myStr = myStr.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g, "");
            myStr = myStr.replace(/\s+/g, "-");

            $('#categoryUrl').val(myStr);
        });

        // $("#addCategoryForm").on('submit', function(e) {
        //     e.preventDefault();

        //     var addCategoryForm = $(this);

        //     $.ajax({
        //         url: addCategoryForm.attr('action'),
        //         type: 'post',
        //         data: addCategoryForm.serialize(),
        //         success: function(response) {
        //             if (response.status == 'success') {
        //                 Snackbar.show({
        //                     text: response.message,
        //                     actionTextColor: '#fff',
        //                     backgroundColor: '#8dbf42'
        //                 });

        //                 $(addCategoryForm)[0].reset();
        //             } else {
        //                 Snackbar.show({
        //                     text: response.message,
        //                     actionTextColor: '#fff',
        //                     backgroundColor: '#e7515a'
        //                 });
        //             }
        //         }
        //     });
        // });

        $(".delete").click(function() {
            event.preventDefault();

            var id = $(this).data('category-id');

            swal({
                title: '<?php echo $this->lang->line('emin_misin'); ?>',
                text: "<?php echo $this->lang->line('geri_alinamaz'); ?>",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '<?php echo $this->lang->line('sil'); ?>',
                padding: '2em'
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: '<?php echo site_url('admin/delete_category') ?>',
                        method: 'POST',
                        data: {
                            id: id
                        },
                        cache: false,
                        dataType: 'JSON',
                        success: function(response) {
                            location.reload();
                        },
                        error: function(data) {
                            $('.alert').removeClass('d-none').text('<?php echo $this->lang->line('kategori_silme_hatasi'); ?>');
                        }
                    });
                }
            })
        });

        $(".edit").click(function() {
            event.preventDefault();

            var id = $(this).data('category-id');

            $.ajax({
                url: '<?php echo site_url('admin/edit_category') ?>',
                method: 'POST',
                data: {
                    id: id
                },
                cache: false,
                dataType: 'JSON',
                success: function(response) {
                    $('#categoryId').val(response.id);
                    $('#categoryName').val(response.title);
                    $('#categoryUrl').val(response.url);
                    $('#metaTitle').val(response.meta_title);
                    $('#metaDescription').val(response.meta_description);
                    $('#metaKeywords').val(response.meta_keywords);
                    $('.return').addClass('d-none');
                    $('.cancel').removeClass('d-none');
                    $('.form').removeAttr('action');
                    $('.form').attr('action', '<?php echo site_url('admin/update_category') ?>');
                    $('#add-tab').trigger('click');
                },
                error: function(data) {
                    $('.alert').removeClass('d-none').text('<?php echo $this->lang->line('bir_hata_olsutu'); ?>');
                }
            });
        });
    </script>

<?php } else if ($this->uri->segment(3) == 'pages') { ?>

    <script src="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/sweetalerts/custom-sweetalert.js"></script>
    <script>
        $(".delete").click(function() {
            event.preventDefault();

            var id = $(this).data('page-id');

            swal({
                title: '<?php echo $this->lang->line('emin_misin'); ?>',
                text: "<?php echo $this->lang->line('geri_alinamaz'); ?>",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '<?php echo $this->lang->line('sil'); ?>',
                padding: '2em'
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: '<?php echo site_url('admin/delete_page') ?>',
                        method: 'POST',
                        data: {
                            id: id
                        },
                        cache: false,
                        dataType: 'JSON',
                        success: function(response) {
                            location.reload();
                        },
                        error: function(data) {
                            $('.alert').removeClass('d-none').text('<?php echo $this->lang->line('sayfa_silme_hatasi'); ?>');
                        }
                    });
                }
            })
        });

        $(".edit").click(function() {
            event.preventDefault();

            var id = $(this).data('page-id');

            $.ajax({
                url: '<?php echo site_url('admin/edit_page') ?>',
                method: 'POST',
                data: {
                    id: id
                },
                cache: false,
                dataType: 'JSON',
                success: function(response) {
                    $('#pageId').val(response.id);
                    $('#pageUrl').val(response.url);
                    $('#metaTitle').val(response.meta_title);
                    $('#metaDescription').val(response.meta_description);
                    $('#metaKeywords').val(response.meta_keywords);
                    $('.return').addClass('d-none');
                    $('.cancel').removeClass('d-none');
                    $('.form').removeAttr('action');
                    $('.form').attr('action', '<?php echo site_url('admin/update_page') ?>');
                    $('#add-tab').trigger('click');
                },
                error: function(data) {
                    $('.alert').removeClass('d-none').text('<?php echo $this->lang->line('bir_hata_olsutu'); ?>');
                }
            });
        });
    </script>

<?php } else if ($this->uri->segment(3) == 'platforms') { ?>

    <script src="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/sweetalerts/custom-sweetalert.js"></script>
    <script>
        $('#platformName').bind('keyup keypress blur', function() {
            var myStr = $(this).val();

            myStr = myStr.toLowerCase();
            myStr = myStr.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g, "");
            myStr = myStr.replace(/\s+/g, "-");

            $('#platformUrl').val(myStr);
        });

        $(".delete").click(function() {
            event.preventDefault();

            var id = $(this).data('platform-id');

            swal({
                title: '<?php echo $this->lang->line('emin_misin'); ?>',
                text: "<?php echo $this->lang->line('geri_alinamaz'); ?>",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: '<?php echo $this->lang->line('sil'); ?>',
                padding: '2em'
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: '<?php echo site_url('admin/delete_platform') ?>',
                        method: 'POST',
                        data: {
                            id: id
                        },
                        cache: false,
                        dataType: 'JSON',
                        success: function(response) {
                            location.reload();
                        },
                        error: function(data) {
                            $('.alert').removeClass('d-none').text('<?php echo $this->lang->line('platform_silme_hatasi'); ?>');
                        }
                    });
                }
            })
        });

        $(".edit").click(function() {
            event.preventDefault();

            var id = $(this).data('platform-id');

            $.ajax({
                url: '<?php echo site_url('admin/edit_platform') ?>',
                method: 'POST',
                data: {
                    id: id
                },
                cache: false,
                dataType: 'JSON',
                success: function(response) {
                    $('#platformId').val(response.id);
                    $('#platformName').val(response.title);
                    $('#platformUrl').val(response.url);
                    $('#platformCategory').val(response.category_id);
                    $('#metaTitle').val(response.meta_title);
                    $('#metaDescription').val(response.meta_description);
                    $('#metaKeywords').val(response.meta_keywords);
                    $('#platformImage').removeAttr('required');
                    // $('.rounded-circle').attr('src', '<?php echo site_url('resources/assets/img/') ?>' + response.image);
                    // $('.avatar-lg').removeClass('d-none');
                    $('.return').addClass('d-none');
                    $('.cancel').removeClass('d-none');
                    $('.form').removeAttr('action');
                    $('.form').attr('action', '<?php echo site_url('admin/update_platform') ?>');
                    $('#add-tab').trigger('click');
                },
                error: function(data) {
                    $('.alert').removeClass('d-none').text('<?php echo $this->lang->line('bir_hata_olsutu'); ?>');
                }
            });
        });
    </script>

<?php } else if ($this->uri->segment(3) == 'accounts') { ?>

    <script src="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/sweetalerts/custom-sweetalert.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/table/datatable/datatables.js"></script>
    <script>
        // $(".delete").click(function() {
        //     event.preventDefault();

        //     var id = $(this).data('platform-id');

        //     swal({
        //         title: 'Are you sure?',
        //         text: "You won't be able to revert this!",
        //         type: 'warning',
        //         showCancelButton: true,
        //         confirmButtonText: 'Delete',
        //         padding: '2em'
        //     }).then(function(result) {
        //         if (result.value) {
        //             $.ajax({
        //                 url: '<?php echo site_url('admin/delete_platform') ?>',
        //                 method: 'POST',
        //                 data: {
        //                     id: id
        //                 },
        //                 cache: false,
        //                 dataType: 'JSON',
        //                 success: function(response) {
        //                     location.reload();
        //                 },
        //                 error: function(data) {
        //                     $('.alert').removeClass('d-none').text('There was a problem while deleting the platform.');
        //                 }
        //             });
        //         }
        //     })
        // });

        $('#zero-config').DataTable({
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 50
        });
    </script>

<?php } else if ($this->uri->segment(2) == 'generator') { ?>

    <script src="<?php echo base_url('resources') ?>/assets/js/scrollspyNav.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/notification/snackbar/snackbar.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/assets/js/components/notification/custom-snackbar.js"></script>
    <script>
        <?php if ($this->session->flashdata('error')) { ?>

            Snackbar.show({
                text: '<?php echo $this->session->flashdata('error'); ?>',
                actionTextColor: '#fff',
                backgroundColor: '#e7515a'
            });

        <?php } ?>
    </script>

<?php } else if ($this->uri->segment(2) == 'generate') { ?>

    <script src="<?php echo base_url('resources') ?>/assets/js/clipboard/clipboard.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/assets/js/forms/custom-clipboard.js"></script>
    <script>
        <?php if (id_user_info($this->session->userdata('id'))->share == 0) { ?>

            $(".btn-instagram").click(function() {
                event.preventDefault();

                $.ajax({
                    url: '<?php echo site_url('locker/get_ig') ?>',
                    method: 'POST',
                    cache: false,
                    dataType: 'JSON',
                    success: function(response) {
                        windowControl = window.open(response.link, 'mywindow', 'width=500, height=500');

                        setInterval(function() {
                            $.ajax({
                                url: '<?php echo site_url('locker/confirm_ig') ?>',
                                method: 'POST',
                                cache: false,
                                dataType: 'JSON',
                                success: function(response) {
                                    if (response.result) {
                                        if (windowControl.closed) {
                                            location.reload();
                                        }
                                    }
                                }
                            });
                        }, 1000);
                    }
                });
            });

            $(".btn-twitter").click(function() {
                event.preventDefault();

                $.ajax({
                    url: '<?php echo site_url('locker/get_tw') ?>',
                    method: 'POST',
                    cache: false,
                    dataType: 'JSON',
                    success: function(response) {
                        windowControl = window.open(response.link, 'mywindow', 'width=500, height=500');

                        setInterval(function() {
                            $.ajax({
                                url: '<?php echo site_url('locker/confirm_tw') ?>',
                                method: 'POST',
                                cache: false,
                                dataType: 'JSON',
                                success: function(response) {
                                    if (response.result) {
                                        windowControl.close();
                                        location.reload();

                                        <?php $this->session->unset_userdata('tw_access_token'); ?>
                                        <?php $this->session->unset_userdata('count'); ?>
                                    }
                                }
                            });
                        }, 1000);
                    }
                });
            });

            $(".btn-facebook").click(function() {
                event.preventDefault();

                $.ajax({
                    url: '<?php echo site_url('locker/get_fb') ?>',
                    method: 'POST',
                    cache: false,
                    dataType: 'JSON',
                    success: function(response) {
                        windowControl = window.open(response.link, 'mywindow', 'width=750, height=600');
                    }
                });

                setInterval(function() {
                    if (windowControl.closed) {
                        location.reload();
                    }
                }, 1000);
            });

            $(".btn-youtube").click(function() {
                event.preventDefault();

                $.ajax({
                    url: '<?php echo site_url('locker/get_yt') ?>',
                    method: 'POST',
                    cache: false,
                    dataType: 'JSON',
                    success: function(response) {
                        windowControl = window.open(response.link, 'mywindow', 'width=750, height=600');
                    }
                });

                setInterval(function() {
                    if (windowControl.closed) {
                        location.reload();
                    }
                }, 1000);
            });

        <?php } ?>
    </script>

<?php } else if ($this->uri->segment(2) == 'user' && $this->uri->segment(3) == 'settings') { ?>

    <script src="<?php echo base_url('resources') ?>/plugins/dropify/dropify.min.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/blockui/jquery.blockUI.min.js"></script>
    <!-- <script src="plugins/tagInput/tags-input.js"></script> -->
    <script src="<?php echo base_url('resources') ?>/assets/js/users/account-settings.js"></script>
    <script src="<?php echo base_url('resources') ?>/assets/js/forms/bootstrap_validation/bs_validation_script.js"></script>
    <script>
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);

        $('[data-pass="hideShow"]').click(function() {
            var dataInputName = $(this).attr('data-input-name');
            if ($('#' + dataInputName).attr('type') == "text") {
                $('#' + dataInputName).attr('type', 'password')
                $(this).html('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off"><path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg>');
            } else {
                $('#' + dataInputName).attr('type', 'text')
                $(this).html('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye">  <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path> <circle cx="12" cy="12" r="3"></circle> </svg>')
            }
        });

        $('#myInput').focus(function() {
            $(this).next('input').focus();
        })
    </script>

<?php } else if ($this->uri->segment(2) == 'mail') { ?>

    <script src="<?php echo base_url('resources') ?>/assets/js/custom.js"></script>
    <script src="<?php echo base_url('resources') ?>/plugins/editors/quill/quill.js"></script>
    <script src="<?php echo base_url('resources') ?>/assets/js/ie11fix/fn.fix-padStart.js"></script>
    <script src="<?php echo base_url('resources') ?>/assets/js/apps/custom-mailbox.js"></script>

<?php } ?>