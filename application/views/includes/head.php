<?php $settings = get_settings(); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

        <?php $this->load->view('includes/meta'); ?>

        <link rel="icon" type="image/x-icon" href="<?php echo base_url('resources/assets/img/') . $settings->favicon; ?>"/>
        <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
        <link href="<?php echo base_url('resources') ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/plugins.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/pages/error/style-400.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url('resources') ?>/plugins/font-icons/fontawesome/css/regular.css">
        <!-- <link rel="stylesheet" href="<?php echo base_url('resources') ?>/plugins/font-icons/fontawesome/css/fontawesome.css"> -->
        <link rel="stylesheet" href="<?php echo base_url('resources') ?>/plugins/font-icons/font-awesome/css/all.min.css">
        <style>
            .layout-px-spacing {
                min-height: calc(100vh - 184px)!important;
            }
        </style>

        <?php $this->load->view('includes/styles'); ?>

    </head>