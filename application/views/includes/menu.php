<?php $settings = get_settings(); ?>

<body class="sidebar-noneoverflow">
    <div class="header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu">
                    <line x1="3" y1="12" x2="21" y2="12"></line>
                    <line x1="3" y1="6" x2="21" y2="6"></line>
                    <line x1="3" y1="18" x2="21" y2="18"></line>
                </svg>
            </a>
            <div class="nav-logo align-self-center">
                <a class="navbar-brand" href="<?php echo site_url(); ?>">
                    <!-- <img alt="logo" src="<?php echo site_url('resources/assets/img/') . $settings->logo; ?>">
                    <img alt="logo" src="https://designreset.com/cork/ltr/demo8/assets/img/logo.svg"> -->
                    <span class="navbar-brand-name"><?php echo $settings->title; ?></span>
                </a>
            </div>
            <ul class="navbar-item flex-row nav-dropdowns ml-auto">
                <li class="nav-item dropdown language-dropdown more-dropdown">
                    <div class="dropdown custom-dropdown-icon">
                        <a class="dropdown-toggle btn" href="#" role="button" id="languageDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="<?php echo base_url(); ?>resources/assets/img/language/<?php echo $this->session->userdata('language'); ?>.png" class="flag-width" alt="flag">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                <polyline points="6 9 12 15 18 9"></polyline>
                            </svg>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right animated fadeInUp" aria-labelledby="customDropdown">
                            <a class="dropdown-item" data-img-value="en" data-value="de" href="<?php echo site_url($this->lang->switch_uri('en')); ?>">
                                <img src="<?php echo base_url(); ?>resources/assets/img/language/en.png" class="flag-width" alt="flag"> 
                                <?php echo $this->lang->line('ingilizce'); ?>
                            </a>
                            <a class="dropdown-item" data-img-value="tr" data-value="de" href="<?php echo site_url($this->lang->switch_uri('tr')); ?>">
                                <img src="<?php echo base_url(); ?>resources/assets/img/language/tr.png" class="flag-width" alt="flag">
                                <?php echo $this->lang->line('turkce'); ?>
                            </a>
                            <a class="dropdown-item" data-img-value="fr" data-value="de" href="<?php echo site_url($this->lang->switch_uri('fr')); ?>">
                                <img src="<?php echo base_url(); ?>resources/assets/img/language/fr.png" class="flag-width" alt="flag">
                                <?php echo $this->lang->line('fransizca'); ?>
                            </a>
                            <a class="dropdown-item" data-img-value="pr" data-value="de" href="<?php echo site_url($this->lang->switch_uri('pr')); ?>">
                                <img src="<?php echo base_url(); ?>resources/assets/img/language/pr.png" class="flag-width" alt="flag">
                                <?php echo $this->lang->line('portekizce'); ?>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown user-profile-dropdown order-lg-0 order-1">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="user-profile-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media">
                            <img src="<?php echo base_url('uploads/user-img/'); ?><?php echo id_user_info($this->session->userdata('id'))->img; ?>" class="rounded-circle">
                            <div class="media-body align-self-center">
                                <h6>
                                    <span><?php echo id_user_info($this->session->userdata('id'))->email; ?></span>
                                </h6>
                            </div>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </a>
                    <div class="dropdown-menu position-absolute animated fadeInUp" aria-labelledby="user-profile-dropdown">
                        <div class="">
                        
                            <?php if (id_user_info($this->session->userdata('id'))->auth == 1) { ?>

                                <div class="dropdown-item">
                                    <a class="" href="<?php echo site_url('admin'); ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-key">
                                            <path d="M21 2l-2 2m-7.61 7.61a5.5 5.5 0 1 1-7.778 7.778 5.5 5.5 0 0 1 7.777-7.777zm0 0L15.5 7.5m0 0l3 3L22 7l-3-3m-3.5 3.5L19 4"></path>
                                        </svg> <?php echo $this->lang->line('yonetici_paneli'); ?>
                                    </a>
                                </div>

                            <?php } ?>

                            <div class="dropdown-item">
                                <a class="" href="<?php echo site_url('user/settings'); ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user">
                                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="12" cy="7" r="4"></circle>
                                    </svg> <?php echo $this->lang->line('hesap_ayarlari'); ?>
                                </a>
                            </div>
                            <div class="dropdown-item">
                                <a class="" href="<?php echo site_url('logout'); ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out">
                                        <path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
                                        <polyline points="16 17 21 12 16 7"></polyline>
                                        <line x1="21" y1="12" x2="9" y2="12"></line>
                                    </svg> <?php echo $this->lang->line('cikis_yap'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <div class="main-container" id="container">
        <div class="overlay"></div>
        <div class="search-overlay"></div>
        <div class="topbar-nav header navbar" role="banner">
            <nav id="topbar">
                <ul class="navbar-nav theme-brand flex-row text-center">
                    <li class="nav-item theme-logo">
                        <a href="index.html">
                            <img src="<?php echo site_url('resources/assets/img/') . $settings->logo; ?>" class="navbar-logo" alt="logo">
                        </a>
                    </li>
                    <li class="nav-item theme-text">
                        <a href="index.html" class="nav-link"><?php echo $settings->title; ?></a>
                    </li>
                </ul>
                <ul class="list-unstyled menu-categories" id="topAccordion">
                    <li class="menu single-menu <?php echo ($this->uri->segment(2) == '' || $this->uri->segment(2) == 'home') ? 'active' : '' ?>">
                        <a href="<?php echo site_url('home'); ?>">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                </svg>
                                <span><?php echo $this->lang->line('home'); ?></span>
                            </div>
                        </a>
                    </li>
                    <li class="menu single-menu <?php echo ($this->uri->segment(2) == 'generator') ? 'active' : '' ?>">
                        <a href="<?php echo site_url('generator'); ?>">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-codesandbox">
                                    <path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                    <polyline points="7.5 4.21 12 6.81 16.5 4.21"></polyline>
                                    <polyline points="7.5 19.79 7.5 14.6 3 12"></polyline>
                                    <polyline points="21 12 16.5 14.6 16.5 19.79"></polyline>
                                    <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                    <line x1="12" y1="22.08" x2="12" y2="12"></line>
                                </svg>
                                <span><?php echo $this->lang->line('hesap_olusturucu'); ?></span>
                            </div>
                        </a>
                    </li>
                    <!-- <li class="menu single-menu <?php echo ($this->uri->segment(1) == 'configs') ? 'active' : '' ?>">
                        <a href="<?php echo site_url('configs'); ?>">
                            <div class="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-cpu">
                                    <rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect>
                                    <rect x="9" y="9" width="6" height="6"></rect>
                                    <line x1="9" y1="1" x2="9" y2="4"></line>
                                    <line x1="15" y1="1" x2="15" y2="4"></line>
                                    <line x1="9" y1="20" x2="9" y2="23"></line>
                                    <line x1="15" y1="20" x2="15" y2="23"></line>
                                    <line x1="20" y1="9" x2="23" y2="9"></line>
                                    <line x1="20" y1="14" x2="23" y2="14"></line>
                                    <line x1="1" y1="9" x2="4" y2="9"></line>
                                    <line x1="1" y1="14" x2="4" y2="14"></line>
                                </svg>
                                <span>Configs</span>
                            </div>
                        </a>
                    </li> -->
                </ul>
            </nav>
        </div>
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
                        <div class="widget-content-area br-4 <?php echo ($this->uri->segment(1) == 'user/settings') ? 'bg-transparent' : ''; ?>">
                            <div class="widget-one">