<?php if ($this->uri->segment(2) == 'register' || $this->uri->segment(2) == 'login' || $this->uri->segment(2) == 'recovery' || $this->uri->segment(2) == 'change-password') { ?>

    <link href="<?php echo base_url('resources') ?>/assets/css/authentication/form-2.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/assets/css/forms/theme-checkbox-radio.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/assets/css/forms/switches.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/assets/css/elements/alert.css">

<?php } else if ($this->uri->segment(2) == 'admin') { ?>

    <link href="<?php echo base_url('resources') ?>/assets/css/elements/infobox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('resources') ?>/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('resources') ?>/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('resources') ?>/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />

    <?php if ($this->uri->segment(3) == 'categories') { ?>

        <link href="<?php echo base_url('resources') ?>/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/assets/css/forms/theme-checkbox-radio.css">
        <link href="<?php echo base_url('resources') ?>/assets/css/tables/table-basic.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/components/custom-sweetalert.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/notification/snackbar/snackbar.min.css" rel="stylesheet" type="text/css" />

    <?php } else if ($this->uri->segment(3) == 'platforms') { ?>

        <link href="<?php echo base_url('resources') ?>/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/assets/css/forms/theme-checkbox-radio.css">
        <link href="<?php echo base_url('resources') ?>/assets/css/tables/table-basic.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css" />

    <?php } else if ($this->uri->segment(3) == 'pages') { ?>

        <link href="<?php echo base_url('resources') ?>/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/assets/css/forms/theme-checkbox-radio.css">
        <link href="<?php echo base_url('resources') ?>/assets/css/tables/table-basic.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/components/custom-sweetalert.css" rel="stylesheet" type="text/css" />

    <?php } else if ($this->uri->segment(3) == 'accounts') { ?>

        <link href="<?php echo base_url('resources') ?>/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/components/custom-sweetalert.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/plugins/table/datatable/datatables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/plugins/table/datatable/dt-global_style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/assets/css/custom.css">

    <?php } else if ($this->uri->segment(3) == 'settings') { ?>

        <link href="<?php echo base_url('resources') ?>/assets/css/elements/avatar.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('resources') ?>/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />

    <?php } ?>

<?php } else if ($this->uri->segment(2) == 'category') { ?>

    <link href="<?php echo base_url('resources') ?>/assets/css/components/cards/card.css" rel="stylesheet" type="text/css" />

<?php } else if ($this->uri->segment(2) == 'platform') { ?>

    <link href="<?php echo base_url('resources') ?>/assets/css/pages/helpdesk.css" rel="stylesheet" type="text/css" />

<?php } else if ($this->uri->segment(2) == 'generator') { ?>

    <link href="<?php echo base_url('resources') ?>/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('resources') ?>/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('resources') ?>/assets/css/components/cards/card.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('resources') ?>/plugins/notification/snackbar/snackbar.min.css" rel="stylesheet" type="text/css" />

<?php } else if ($this->uri->segment(2) == 'generate') { ?>

    <link href="<?php echo base_url('resources') ?>/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('resources') ?>/assets/css/forms/custom-clipboard.css">

<?php } else if ($this->uri->segment(2) == 'user' && $this->uri->segment(3) == 'settings') { ?>

    <link href="<?php echo base_url('resources') ?>/plugins/dropify/dropify.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('resources') ?>/assets/css/users/account-setting.css" rel="stylesheet" type="text/css" />

<?php } else if ($this->uri->segment(2) == 'mail') { ?>

    <link href="<?php echo base_url('resources') ?>/assets/css/apps/mailbox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('resources') ?>/plugins/editors/quill/quill.snow.css" rel="stylesheet" type="text/css" />


<?php } ?>