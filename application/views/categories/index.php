<?php

$this->load->view('includes/head');
$this->load->view('includes/menu');

?>

<h3><?php echo $title; ?></h3>
<div class="row mb-3 mt-3">

    <?php foreach ($categories->result() as $category) { ?>

    <div class="col-12 col-lg-3 col-md-4 col-sm-6 mb-4 mb-md-0">
        <div class="card component-card_2 w-100">
            <img src="<?php echo base_url('resources/assets/img/') . $category->image; ?>" class="card-img-top" alt="widget-card-2">
            <div class="card-body text-center">
                <a href="<?php echo site_url('generate/') . $category->url; ?>" class="btn btn-primary"><?php echo $this->lang->line('olustur'); ?></a>
            </div>
        </div>
    </div>

    <?php } ?>

</div>

<?php $this->load->view('includes/footer'); ?>