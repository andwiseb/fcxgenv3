<?php

$this->load->view('includes/head');
$this->load->view('includes/menu');

?>

<!-- Profil Fotoğrafı Değiştirme -->
<!-- Şifre Değiştirme -->
<!-- Email Değiştirme -->
<h3><?php echo $title; ?></h3>

<?php if ($this->session->flashdata('settings_message')) { ?>

    <div class="alert alert-danger mb-4 mt-4" role="alert"><?php echo $this->session->flashdata('settings_message'); ?></div>

<?php } ?>

<div class="account-settings-container layout-top-spacing">
    <div class="account-content">
        <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                    <form id="needs-validation general-info <?php if ($this->session->flashdata('settins_wrong_password')) { echo 'was-validated'; } ?>" novalidate class="section general-info" method="POST" action="<?php echo site_url('user/change_general') ?>" enctype="multipart/form-data" >
                        <div class="info">
                            <h6 class=""><?php echo $this->lang->line("genel_ayarlar"); ?></h6>
                            <div class="row">
                                <div class="col-lg-11 mx-auto">
                                    <div class="row mb-4">
                                        <div class="col-xl-2 col-lg-12 col-md-4">
                                            <div class="upload mt-4 pr-md-4">
                                                <input type="file" id="input-file-max-fs" name="changePicture" class="dropify" data-default-file="<?php echo base_url('uploads/user-img/') . $settings->img; ?>" data-max-file-size="2M" />
                                                <p class="mt-2">
                                                    <i class="flaticon-cloud-upload mr-1"></i> <?php echo $this->lang->line("profil_resmi_yukle"); ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-10 mt-md-0 mt-4">
                                            <div class="form w-100">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="old_email"><?php echo $this->lang->line("eposta"); ?></label>
                                                            <input type="email" class="form-control mb-4" id="email" name="old_email" value="<?php echo $settings->email; ?>" readonly disabled">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="new-mail"><?php echo $this->lang->line("yeni_eposta"); ?></label>
                                                            <input type="email" class="form-control mb-4" name="new_mail" id="new-mail" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label for="password-mail"><?php echo $this->lang->line("sifre"); ?></label>
                                                        <div class="input-group">
                                                            <input type="password" class="form-control" name="password_mail" id="password-mail" aria-describedby="mailPassword" required>
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" data-pass="hideShow" id="mailPassword" data-input-name="password-mail">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off">
                                                                        <path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path>
                                                                        <line x1="1" y1="1" x2="23" y2="23"></line>
                                                                    </svg>
                                                                </span>
                                                            </div>
                                                            <div class="invalid-tooltip">
                                                                <?php echo $this->lang->line("alan_doldurma_hatasi"); ?>
                                                            </div>
                                                            <?php if ($this->session->flashdata('settins_wrong_password')) { ?>
                                                                <div class="invalid-tooltip">
                                                                    <?php echo $this->session->flashdata('settins_wrong_password'); ?>
                                                                </div>

                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 text-right mb-4">
                                                        <button type="submit" name="generalChange" id="multiple-messages" class="btn btn-primary"><?php echo $this->lang->line('kaydet'); ?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                    <form class="needs-validation section about" novalidate method="POST" action="<?php echo site_url('user/change_password') ?>">
                        <div class="info">
                            <h5 class=""><?php echo $this->lang->line("sifre_degistir"); ?></h5>
                            <div class="row">
                                <div class="col-md-11 mx-auto">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="current-password"><?php echo $this->lang->line("mevcut_sifre"); ?></label>
                                            <div class="input-group">
                                                <input type="password" class="form-control" name="current_password" id="current-password" aria-describedby="currentPassword" required>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" data-pass="hideShow" id="currentPassword" data-input-name="current-password">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off">
                                                            <path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path>
                                                            <line x1="1" y1="1" x2="23" y2="23"></line>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="invalid-tooltip">
                                                    <?php echo $this->lang->line("alan_doldurma_hatasi"); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="new-password"><?php echo $this->lang->line("yeni_sifre"); ?></label>
                                            <div class="input-group">
                                                <input type="password" class="form-control" name="new_password" id="new-password" aria-describedby="newPassword" required>
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" data-pass="hideShow" data-input-name="new-password">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off">
                                                            <path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path>
                                                            <line x1="1" y1="1" x2="23" y2="23"></line>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="invalid-tooltip">
                                                    <?php echo $this->lang->line("alan_doldurma_hatasi"); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 mb-3">
                                            <label for="re-new-password"><?php echo $this->lang->line("sifreyi_onayla_2"); ?></label>
                                            <div class="input-group">
                                                <input type="password" class="form-control" name="re_new_password" id="re-new-password" aria-describedby="confirmNewPassword" required>
                                                <div class="input-group-prepend">
                                                    <span type="button" class="input-group-text" data-pass="hideShow" data-input-name="re-new-password">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off">
                                                            <path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path>
                                                            <line x1="1" y1="1" x2="23" y2="23"></line>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div class="invalid-tooltip">
                                                    <?php echo $this->lang->line("alan_doldurma_hatasi"); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 text-right mb-4">
                                            <button type="submit" name="passChange" class="btn btn-primary"><?php echo $this->lang->line("kaydet"); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>