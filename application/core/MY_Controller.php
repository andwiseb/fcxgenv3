<?php

class MY_Controller extends CI_Controller {
	public function __construct() {
        parent::__construct();
        
        header('Access-Control-Allow-Origin: *');

        $this->load->helper('main_helper');

        if ($this->uri->segment(1) == 'en' || $this->uri->segment(1) == '') {
            $this->session->set_userdata('language', 'en');
        } else {
            $this->session->set_userdata('language', $this->uri->segment(1));
        }

        if ($this->session->userdata('language')) {
            $this->load->language($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->session->set_userdata('language', 'en');
        }

        if (id_user_info($this->session->userdata('id'))->verify == 2) {
            if ($this->uri->segment(2) != 'user' && $this->uri->segment(2) != 'change-email') {
                $this->session->set_flashdata('settings_message', $this->lang->line('eposta_degistirin'));

                redirect(site_url('user/settings'));
            }
        }
	}
}
